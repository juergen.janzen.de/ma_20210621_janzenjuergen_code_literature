from matplotlib import pyplot as plt
from clodsa.augmentors.augmentorFactory import createAugmentor
from clodsa.transformers.transformerFactory import transformerGenerator
from clodsa.techniques.techniqueFactory import createTechnique
import cv2
from PrepJson import cleanjson
from pathlib import Path
import os

def createcrop(stride:int, size:int, image_size:tuple):
    """
    Creates a list of crop augmentations according to the
    stride and size of the crop and the image size (width, hight).
    Be aware there is no padding! If the image can't be completly
    croped an error occures.
    """

    # checking if cropping without padding is possible
    wcheck = (image_size[0] - size) % stride
    hcheck = (image_size[1] - size) % stride

    assert wcheck == 0, "Can't crop proper in width"
    assert hcheck == 0, "Can't crop proper in hight"

    crops = (((image_size[0] - size) / stride)+1) * (((image_size[1] - size) / stride)+1) #number of crops

    croplist = [] # list of configured function 'createTechnique'
    x = 0
    y = 0
    
    for _ in range(int(crops)):
        croplist.append(createTechnique("cropSize",{"x": x,"y": y, "width": size, "height": size}))

        if (image_size[0] - (x+size)) > 0:
            x += stride
        else:
            x = 0
            y += stride

    return croplist

def createflip():
    """
    Creates a list of flip augmentations
    """
    fliplist = []
    fliplist.append(createTechnique("flip", {"flip": 0}))
    fliplist.append(createTechnique("flip", {"flip": 1}))
    return fliplist

def createrot(angle:list):
    """
    converts the list of angle in corresponding
    rot augmentations
    """

    rotlist = []
    for rot in angle:
        rotlist.append(createTechnique("rotate", {"angle": rot}))

    return rotlist

def main():
    """
    Before this can be used make sure the annotation file and images are in the same folder. Furthermore apply PrepJson.py before this one to make sure the JSON-file is complete and clean. If still an error occures just run the PrepJson.py multiple times (up to 3 or 4 times might be necessary)
    """

    # configure CloDSA
    PROBLEM = "instance_segmentation"
    ANNOTATION_MODE = "coco"
    ORG_INPUT_PATH = "/home/jjuergen/Desktop/masterarbeit/Bilder/25_Grillenoriginal"
    GENERATION_MODE = "linear"
    OUTPUT_MODE = "coco"
    PATH_DATASETS = "/home/jjuergen/Desktop/masterarbeit/Bilder/25_train"
    image_size = (2048, 2048)
    CLODSA_FILE = "annotation.json"
    DATASET = "Acheta domesticus"

    # (size, stride, name); unit is px
    croppairs = [
        (257, 199, "crop1225"),
        (256, 128, "crop1250"),
        (256, 64, "crop1275"),
        (512, 384, "crop2525"),
        (512, 256, "crop2550"),
        (512, 128, "crop2575"),
        (1138, 910, "crop5025"),
        (1024, 512, "crop5050"),
        (1024, 256, "crop5075"),
    ]

    angle = [90, 180, 270] #angle in degrees

    cropdic = dict()

    for pair in croppairs:
        cropdic[pair[2]] = dict()
        cropdic[pair[2]]["size"] = pair[0]
        cropdic[pair[2]]["stride"] = pair[1]

    for crop in cropdic:
        INPUT_PATH = ORG_INPUT_PATH
        PARENT_PATH = PATH_DATASETS + "/" + crop
        Path(PARENT_PATH).mkdir(parents=True, exist_ok=True)

        # Cropping
        OUTPUT_PATH = PARENT_PATH + "/None"
        Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)
        OUTPUT_PATH = OUTPUT_PATH + "/"
        augmentor = createAugmentor(PROBLEM,ANNOTATION_MODE,OUTPUT_MODE,GENERATION_MODE,INPUT_PATH,{"outputPath":OUTPUT_PATH})
        transformer = transformerGenerator(PROBLEM)

        croppings = createcrop(cropdic[crop]["stride"], cropdic[crop]["size"], image_size)
        for cropaug in croppings:
            augmentor.addTransformer(transformer(cropaug))

        augmentor.applyAugmentation()
        DATASET_PARENT =  DATASET + " " + crop

        cleanjson(OUTPUT_PATH, CLODSA_FILE, DATASET_PARENT)
        cleanjson(OUTPUT_PATH, CLODSA_FILE, DATASET_PARENT, clodsa=True)

        # Flipping
        flippings = createflip()
        INPUT_PATH = OUTPUT_PATH[:len(OUTPUT_PATH)-1]
        OUTPUT_PATH = PARENT_PATH + "/FlipHori"
        Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)
        OUTPUT_PATH = OUTPUT_PATH + "/"
        augmentor = createAugmentor(PROBLEM,ANNOTATION_MODE,OUTPUT_MODE,GENERATION_MODE,INPUT_PATH,{"outputPath":OUTPUT_PATH})
        transformer = transformerGenerator(PROBLEM)

        augmentor.addTransformer(transformer(flippings[0]))
        augmentor.applyAugmentation()
        DATASET_CHILD =  DATASET_PARENT + " FlipHori"
        cleanjson(OUTPUT_PATH, CLODSA_FILE, DATASET_CHILD)

        OUTPUT_PATH = PARENT_PATH + "/FlipVert"
        Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)
        OUTPUT_PATH = OUTPUT_PATH + "/"
        augmentor = createAugmentor(PROBLEM,ANNOTATION_MODE,OUTPUT_MODE,GENERATION_MODE,INPUT_PATH,{"outputPath":OUTPUT_PATH})
        transformer = transformerGenerator(PROBLEM)

        augmentor.addTransformer(transformer(flippings[1]))
        augmentor.applyAugmentation()
        DATASET_CHILD =  DATASET_PARENT + " FlipVert"
        cleanjson(OUTPUT_PATH, CLODSA_FILE, DATASET_CHILD)

        # Rotating
        rotations = createrot(angle)
        for aug, ang in zip(rotations, angle):
            OUTPUT_PATH = PARENT_PATH + "/Rot" + str(ang)
            Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)
            OUTPUT_PATH = OUTPUT_PATH + "/"
            augmentor = createAugmentor(PROBLEM,ANNOTATION_MODE,OUTPUT_MODE,GENERATION_MODE,INPUT_PATH,{"outputPath":OUTPUT_PATH})
            transformer = transformerGenerator(PROBLEM)

            augmentor.addTransformer(transformer(aug))
            augmentor.applyAugmentation()
            DATASET_CHILD =  DATASET_PARENT + " Rot" + str(ang)
            cleanjson(OUTPUT_PATH, CLODSA_FILE, DATASET_CHILD)

if __name__== "__main__":
    main()