import json
import os
from copy import deepcopy
from datetime import date

def cleanjson(path, input_jason, dataset_description="", version="1.0", url="", clodsa=False):
    """
    The json file from the "Coco Annotator" isn't complete and therefor CLODSA and MSRCNN would return errors. This function adds "info" and "licenses" to the file and removes any annotations which have less than 3 support vertices. 
    if clodsa = False:
    Two json files will be saved, one with the categories of the 
    original as "allinsect.json" and one where all annotations are of category "insect" as "onlyinsect.json"
    if clodsa = True in order to use CLODSA:
    only the "allinsect.json" will be saved as "annotations.json"
    """
    # load json file
    with open(path + input_jason) as data_file:
                cricket = json.load(data_file)

    # add "info" if not already in json
    if not ("info" in cricket):
        today = date.today()
        year = today.year
        today = today.strftime("%d/%m/%Y")
        year = str(year)
        
        cricket["info"] = {
                                "description": dataset_description,
                                "url": url,
                                "version": version,
                                "year": year,
                                "contributor": "J.Janzen",
                                "date_created": today
                            }

    # adds "licenses" if not already in json
    if not ("licenses" in cricket):
        cricket["licenses"] = [
                        {
                            "url": "http://creativecommons.org/publicdomain/zero/1.0/",
                            "id": 1,
                            "name": "Public Domain Dedication (CC Zero)"
                        }
                    ]

        for i in range(len(cricket["images"])):
            cricket["images"][i]["license"] = 1

    # checks the annotations
    for annot in cricket["annotations"]:
        
        if (len(annot["segmentation"][0]) <= 4) or (annot["area"] == 0.0):
            cricket["annotations"].remove(annot)

    # allinsect.json is ready to be saved now to
    # onlyinsect.json
    org_cricket = deepcopy(cricket)

    cat = {
        "id": 99,
        "name": "insect",
        "supercategory": "",
        "color": "#01b5ce",
        "metadata": {},
        "keypoint_colors": []
    }

    cricket["categories"] = []
    cricket["categories"].append(cat)

    # changing all categories to insect
    for annot in cricket["annotations"]:
        idx = cricket["annotations"].index(annot)
        cricket["annotations"][idx]["category_id"] = cat["id"]

    # save the json-files
    # if it's only for using CLODSA than there is no need to save 
    # the other json files
    if clodsa:
        with open(path + "annotations.json", 'w') as outfile:
                json.dump(org_cricket, outfile)
    else:
        with open(path + "allinsect.json", 'w') as outfile:
                    json.dump(org_cricket, outfile)

        with open(path + "onlyinsect.json", 'w') as outfile:
                    json.dump(cricket, outfile)

def main():
    """
    For splitting the Dataset in train, test and validation set. Removes the image and its annotations in the json. Put the original json and the selected images in one folder and run this code
    """
    path = "/home/jjuergen/Desktop/masterarbeit/Bilder/25_Grillenoriginal"

    img_list=list()
    for f in os.listdir(path):
        if 'jpg' in f:
            img_list.append(f)

    for f in os.listdir(path):
        if 'json' in f:
            with open(os.path.join(path, f),'r') as inputfile:
                cricket = json.load(inputfile)

            image_id=list()
            im = True
            while im:
                for c in cricket["images"]:
                    if c["file_name"] not in img_list:
                        cricket["images"].remove(c)
                    else:
                        image_id.append(c["id"])
                
                for c in cricket["images"]:
                    if c["file_name"] not in img_list:
                        im = True
                        break
                    else:
                        im = False
            t=True
            count=0
            while t or count>20:
                count +=1
                for a in cricket["annotations"]:
                    if a['image_id'] not in image_id:
                        cricket["annotations"].remove(a)

                t=False
                for at in cricket["annotations"]:
                    if at['image_id'] not in image_id:
                        t=True

            if count>20:
                print(True)

            with open(os.path.join(path, f),'w') as outfile:
                json.dump(cricket, outfile)

if __name__ == "__main__":
    main()
