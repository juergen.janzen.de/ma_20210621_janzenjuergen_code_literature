from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt
import pylab
import cv2

def main():
    """
    Shows the annotations by using pycocotools. To have influace on the colour of the annotations, you need make the changes directly in the code of pycocotools.coco.COCO.
    """

    image_directory = '/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/datasets/testing_out/' # needs to end with '/'
    annotation_file = '/home/jjuergen/Desktop/masterarbeit/Bilder/Validation/onlyinsect.json' #doesn't need to end with '/'

    example_coco = COCO(annotation_file)

    categories = example_coco.loadCats(example_coco.getCatIds())
    category_names = [category['name'] for category in categories]
    print('Custom COCO categories: \n{}\n'.format(' '.join(category_names)))

    category_names = set([category['supercategory'] for category in categories])
    print('Custom COCO supercategories: \n{}'.format(' '.join(category_names)))

    category_ids = example_coco.getCatIds(catNms=['insect'])
    image_ids = example_coco.getImgIds(catIds=category_ids)
    image_data = example_coco.loadImgs(image_ids[np.random.randint(0, len(image_ids))])[0]

    image = io.imread(image_directory + image_data['file_name'])
    plt.imshow(image)
    plt.axis('off')
    pylab.rcParams['figure.figsize'] = (8.0, 10.0)
    annotation_ids = example_coco.getAnnIds(imgIds=image_data['id'], catIds=category_ids, iscrowd=None)
    annotations = example_coco.loadAnns(annotation_ids)

    example_coco.showAnns(annotations)

    plt.show()

if __name__=="__main__":
    main()