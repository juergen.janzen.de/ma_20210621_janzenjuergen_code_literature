import json
import matplotlib.pyplot as plt
from numpy import std, mean, array, linalg, median
import os
import operator


def main01():
    jason_path = '/home/jjuergen/Desktop/masterarbeit/Bilder/Grillenoriginal/AchetaDomesticusProduction.json'
    with open(jason_path) as data_file:
                cricket = json.load(data_file)

    width = []
    hight = []
    x_pos = []
    y_pos = []
    dist_min_list = []
    for image in cricket['images']:
        image_id = image['id']
        for annot in cricket['annotations']:
            if annot['image_id'] == image_id:
                x_list = [annot['segmentation'][0][i] for i in range(0,len(annot['segmentation'][0]),2)]
                y_list = [annot['segmentation'][0][j] for j in range(1,len(annot['segmentation'][0])+1,2)]
                x_pos.append(mean(x_list))
                y_pos.append(mean(y_list))
            width.append(annot['bbox'][2])
            hight.append(annot['bbox'][3])
        
        for x_org,y_org in zip(x_pos,y_pos):
            dist_old = 2048
            point_org = array((x_org,y_org))

            for x,y in zip(x_pos,y_pos):
                point = array((x,y))
                dist = linalg.norm(point_org-point)
                if (dist > 0) and (dist<dist_old):
                    dist_old = dist
            
            dist_min_list.append(int(dist_old))

    plot_width = [[], []]
    plot_hight = [[], []]
    for i in range(min(width), max(width)+1):
        if width.count(i) != 0:
            plot_width[0].append(width.count(i)/(i*len(width)))
            plot_width[1].append(i)

    for i in range(min(hight), max(hight)+1):
        if hight.count(i) != 0:
            plot_hight[0].append(hight.count(i)/(i*len(width)))
            plot_hight[1].append(i)
    

    plot_dist = [[], []]
    for i in range(min(dist_min_list), max(dist_min_list)+1):
        if dist_min_list.count(i) != 0:
            plot_dist[0].append(dist_min_list.count(i)/(i*len(dist_min_list)))
            plot_dist[1].append(i)

    print("Mean minimal distant: {0}".format(mean(dist_min_list)))
    print("STD minimal distant: {0}".format(std(dist_min_list)))
    print('Mean Bbox: {0}, {1}'.format(mean(width), mean(hight)))
    print('STD Bbox: {0}, {1}'.format(std(width), std(hight)))
    print(median(width))
    print(median(hight))
    exit()

    plt.figure('prop_width')
    plt.xlim(left=0, right= max([1.05*max(plot_width[1]), 1.05*max(plot_hight[1])]))
    plt.ylim(bottom=0,top=max([1.05*max(plot_width[0]), 1.05*max(plot_hight[0])]) )
    titel = 'Propebility Density Function of width of the Bbox'
    plt.title(titel)
    plt.xlabel('Width in px')
    plt.ylabel('Probability Density')
    plt.plot(plot_width[1],plot_width[0],'.')

    plt.figure('prop_hight')
    plt.xlim(left=0, right= max([1.05*max(plot_width[1]), 1.05*max(plot_hight[1])]))
    plt.ylim(bottom=0,top=max([1.05*max(plot_width[0]), 1.05*max(plot_hight[0])]) )
    titel = 'Propebility Density Function of hight of the Bbox'
    plt.title(titel)
    plt.xlabel('Hight in px')
    plt.ylabel('Probability Density')
    plt.plot(plot_hight[1],plot_hight[0],'.')

    plt.figure('prob_dist')
    plt.axis([0.95*min(plot_dist[1]),1.05*max(plot_dist[1]),0,1.05*max(plot_dist[0])])
    titel = 'Propebility Density Function of minimal distance'
    plt.title(titel)
    plt.xlabel('distance in px')
    plt.ylabel('Probability Density')
    plt.plot(plot_dist[1],plot_dist[0],'.')

    plt.show()

def crop_to_num_annot(json_path):
    
    with open(json_path) as f:
        cricket = json.load(f)

    annot_per_image = []
    for image in cricket["images"]:
        count = 0
        for annot in cricket["annotations"]:
            if annot["image_id"] == image["id"]:
                count += 1
        if count > 0:
            annot_per_image.append(count)

    return [cricket["images"][0]["width"], annot_per_image]

def ratio_fg_bg_per_img(json_path):
    with open(json_path) as f:
        cricket = json.load(f)

    ratio = []
    for image in cricket["images"]:
        fg = 0
        size = image['width']*image['height']
        for annot in cricket["annotations"]:
            if annot["image_id"] == image["id"]:
                fg += annot['area']

        if fg > 0:
            ratio.append(fg/(size-fg))
    
    return [cricket["images"][0]["width"], ratio]

def ratio_fg_bg_overall(json_path):
    with open(json_path) as f:
        cricket = json.load(f)

    fg = 0
    size = 0
    for image in cricket["images"]:
        fg_old = fg
        for annot in cricket["annotations"]:
            if annot["image_id"] == image["id"]:
                fg += annot['area']
        
        if fg_old != fg:
            size += image['width']*image['height']

    ratio = fg/(size-fg)
    
    return [cricket["images"][0]["width"], ratio]

def main02():
    path = "/home/jjuergen/Desktop/masterarbeit/Bilder/Datasets"
    default_folder = "None"
    json_file = "onlyinsect.json"

    points = []
    ratios = []
    ratio_over = []
    for crop in os.listdir(path):
        json_path = os.path.join(path,crop)
        json_path = os.path.join(json_path,default_folder)
        json_path = os.path.join(json_path,json_file)
        points.append(crop_to_num_annot(json_path))
        ratios.append(ratio_fg_bg_per_img(json_path))
        ratio_over.append(ratio_fg_bg_overall(json_path))


    #points = sorted(points, key=operator.itemgetter(0))
    # x = [point[0] for point in points]
    # means = [point[1] for point in points]
    # stds = [point[2] for point in points]
    for i in range(len(points)):
        if points[i][0] == 257:
            points[i][0] = 256
        if points[i][0] == 1138:
            points[i][0] = 1024

    x = [point[0] for point in points]
    x = sorted(list(set(x)))
    means = []
    stds = []
    std_overlap = []
    mean_overlap = []
    for x_pos in x:
        mean_overlap_list = []
        std_overlap_list = []
        check = True
        
        for point in points:
            if point[0] == x_pos:
                mean_overlap_list.append(mean(point[1]))
                std_overlap_list.append(std(point[1]))
                if check:
                    means.append(mean(point[1]))
                    mean_factor = 100/mean(point[1])
                    stds.append(std(point[1]))
                    std_factor = 100/std(point[1])
                    check = False

        std_overlap.append(std(std_overlap_list)*std_factor)
        mean_overlap.append(std(mean_overlap_list)*mean_factor)

    print(mean_overlap)
    print(std_overlap)

    plt.rcParams['font.size'] = '10'
    plt.ylim(bottom=0)
    plt.xlim(left=0)
    plt.figure('annot_per_img',figsize=[6, 4])
    # plt.plot(x,means,label = "means")
    # plt.plot(x,stds,label="std")
    plt.errorbar(x,means,stds,marker='x')
    plt.xlabel('Crop Size')
    plt.ylabel('Annotations per Image')
    plt.grid()

    #ratios = sorted(ratios, key=operator.itemgetter(0))
    # x = [ratio[0] for ratio in ratios]
    # means = [ratio[1] for ratio in ratios]
    # stds = [ratio[2] for ratio in ratios]
    for i in range(len(ratios)):
        if ratios[i][0] == 257:
            ratios[i][0] = 256
        if ratios[i][0] == 1138:
            ratios[i][0] = 1024

    x = [ratio[0] for ratio in ratios]
    x = sorted(list(set(x)))
    means = []
    stds = []
    std_overlap = []
    mean_overlap = []
    for x_pos in x:
        mean_overlap_list = []
        std_overlap_list = []
        check = True
        
        for ratio in ratios:
            if ratio[0] == x_pos:
                mean_overlap_list.append(mean(ratio[1]))
                std_overlap_list.append(std(ratio[1]))
                if check:
                    means.append(mean(ratio[1]))
                    mean_factor = 100/mean(ratio[1])
                    stds.append(std(ratio[1]))
                    std_factor = 100/std(ratio[1])
                    check = False

        std_overlap.append(std(std_overlap_list)*std_factor)
        mean_overlap.append(std(mean_overlap_list)*mean_factor)
    
    print(mean_overlap)
    print(std_overlap)

    plt.rcParams['font.size'] = '10'
    plt.ylim(bottom=0)
    plt.xlim(left=0)
    plt.figure('ratio_fg_bg_per_img', figsize=[6, 4])
    # plt.plot(x,means,label = "means")
    # plt.plot(x,stds,label="std")
    plt.errorbar(x,means,stds,marker='x')
    plt.xlabel('Crop Size')
    plt.ylabel('Fg/Bg per Image')
    plt.grid()

    #ratio_over = sorted(ratio_over, key=operator.itemgetter(0))
    for i in range(len(ratio_over)):
        if ratio_over[i][0] == 257:
            ratio_over[i][0] = 256
        if ratio_over[i][0] == 1138:
            ratio_over[i][0] = 1024

    x = [ratio[0] for ratio in ratio_over]
    x = sorted(list(set(x)))
    means = []
    stds = []
    for x_pos in x:
        value_list = []
        for ratio in ratio_over:
            if ratio[0] == x_pos:
                value_list.append(ratio[1])
        
        means.append(mean(value_list))
        stds.append(std(value_list))

    plt.rcParams['font.size'] = '10'
    plt.ylim(bottom=0)
    plt.xlim(left=0)
    plt.figure('ratio_fg_bg_overall', figsize=[6, 4])
    plt.errorbar(x,means,stds, marker = 'x')
    plt.xlabel('Crop Size')
    plt.ylabel('Fg/Bg per Dataset')
    plt.grid()
    plt.show()
    

if __name__ == "__main__":
    main01()
    #main02()