import matplotlib.pyplot as plt
import numpy as np

color = '#00549f'
labely = 'CoG'
labelx = 'F1-score'

plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [6.4,4],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
})

f1 = [0.458,0.248,0.18]
cog = [0.776, 0.552, 0.502]
i = list(range(1,9))

plt.plot(f1, cog,'o', color=color)
for j,f,c in zip(i, f1, cog):
    plt.annotate('({0})'.format(j), (f,c), xytext=(5,0), textcoords='offset points')
plt.ylabel(labely)
plt.ylim(0,1)
plt.yticks(list(np.arange(0,1.1,0.1)))

plt.xlabel(labelx)
plt.xlim(0,1)
plt.xticks(list(np.arange(0,1.1,0.1)))

plt.grid()

plt.show()