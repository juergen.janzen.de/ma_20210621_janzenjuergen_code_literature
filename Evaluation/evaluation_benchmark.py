#import torch
import os
import matplotlib.pyplot as plt
from operator import itemgetter
import json
import numpy as np
#from adjustText import adjust_text

def make_save_figure(save_path, file_name, titel, xlabel, ylabel, x_list, y_list):
    plt.figure(file_name, figsize=[6, 4])
    plt.title(titel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x_list, y_list, marker='.')
    plt.xlim(left=0, right=max(x_list))
    plt.ylim(bottom=0, top=1)
    plt.grid()
    plt.savefig(os.path.join(save_path, file_name))
    plt.close()

def roc_costum(metric_list, num_gt):
    score_range = list(np.arange(0,1.1,0.1).astype(np.float16))
    precision_list = [None] * len(score_range)
    recall_list = [None] * len(score_range)
    for i, score in enumerate(score_range):
        new_metric_list = [value_dict for value_dict in metric_list if value_dict['score']>=score]
        if len(new_metric_list) > 0:
            precision_list[i], recall_list[i] = avg_prec_rec(new_metric_list, len(new_metric_list), num_gt)

        else:
            precision_list.remove(None)
            recall_list.remove(None)
            score_range.remove(score)

    return precision_list, recall_list, score_range

def konfusion_matrix(metric_list, num_gt, score_threshold, metric_threshold=0):
    actual_metric_list = [metric for metric in metric_list if metric['score'] >= score_threshold]
    metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    tp = 0
    fp = 0
    # maxi = [-1]
    # mini = [2]
    for metric in actual_metric_list:
        if metric_key == 'cog':
            gate = metric[metric_key] <= metric_threshold
        else:
            # maxi = [metric[metric_key] if maxi[0] <metric[metric_key] else maxi[0]]
            # mini = [metric[metric_key] if mini[0] > metric[metric_key] else mini[0]]
            gate = metric[metric_key] >= metric_threshold


        if gate:
            tp += 1
        else:
            fp += 1

    fn = num_gt - tp
    # print('score:{0} /min:{1} /max:{2}'.format(score_threshold, mini, maxi))
    # input()

    return tp, fp, fn

def avg_prec_rec(metric_list, num_d, num_gt, metric_thresholds=None, invert=False):
    if metric_thresholds is None:
        metric_thresholds = np.arange(0.5,1,0.05)

    recall_values = np.arange(0,1.01,0.01)

    metric_list.sort(key=itemgetter('score'), reverse=True)
    metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    assert len(metric_list) == num_d, '{0}, {1}'.format(len(metric_list), num_d)
    prec_list = list()
    rec_list = list()
    for threshold in metric_thresholds:
        temp_dict = dict()
        temp_dict['precision'] = list()
        temp_dict['recall'] = list()
        tp = 0
        for i, detection in enumerate(metric_list):
            if not invert:
                if detection[metric_key] >= threshold:
                    tp += 1
            else:
                if detection[metric_key] <= threshold:
                    tp += 1
            assert tp <= num_gt
            temp_dict['precision'].append(tp/(i+1))
            temp_dict['recall'].append(tp/num_gt)
        
        precision = 0
        for interpol in recall_values:
            to_consider = [temp_dict['precision'][i] for i,rec in enumerate(temp_dict['recall']) if rec>=interpol]
            if len(to_consider) > 0:
                precision += max(to_consider)

        prec_list.append(precision/len(recall_values))
        rec_list.append(max(temp_dict['recall']))

    avg_prec = sum(prec_list)/len(metric_thresholds)
    avg_rec = sum(rec_list)/len(metric_thresholds)

    return avg_prec, avg_rec

def F_score(beta, precision, recall):
    if precision == 0 and recall == 0:
        score = 0
    else:
        score = (1+beta**2) * (precision*recall) / (beta**2 * precision + recall)
    return score    

def main():

    color= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1']
    labelx = 'False positives'
    labely = 'True positives'
    labels= {
            'None': 'Dataset 1',
            'Rot180FlipVert': 'Dataset 2',
            'FlipHoriRot90Rot270':'Dataset 3',
            'Rot90Rot270FlipVert':'Dataset 4'
        }
    symbols = ['.', '+', 'x', '^']
    learning_lib = dict()

    plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [4.2,2.7],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
    })

    os.system('cls')
    path = "D:\Studium\Master_RWTH\msrcnn/validations/ChoosenOnes"
    dataset = "coco_test_insect"
    metirc_file = "eval_metric.json"
    beta = 1
    combo = 'Rot180FlipVert'

    score_thresh = np.arange(0,1.01,0.01)

    for crops in sorted(os.listdir(path), reverse=False):
        crop_path = os.path.join(path,crops)

        for combi in sorted(os.listdir(crop_path), key=len):
            if combi!= combo:
                continue

            combi_path = os.path.join(crop_path, combi, dataset)
            json_file = os.path.join(combi_path, metirc_file)
            
            with open(json_file) as f:
                metric_dict = json.load(f)

            for metric in metric_dict:
                if metric in ('IOG', 'IOU'):
                    continue
                elif metric == 'F1':
                    metric_thr = 0.6
                else:
                    metric_thr = 1000

                for it, it_dict in sorted(metric_dict[metric].items(), key=lambda item: int(item[0])):

                    for _, img_dict in it_dict.items():

                        for cat, cat_dict in img_dict.items():
                            # cat_dict['result'])
                            # cat_dict['num_gt']
                            true_pos = list()
                            false_pos = list()
                            false_neg = list()
                            result = cat_dict['result']
                            num_gt = cat_dict['num_gt']
                            for thr in score_thresh:
                                tp, fp, fn = konfusion_matrix(result,num_gt , thr, metric_thr)
                                true_pos.append(tp)
                                false_pos.append(fp)
                                false_neg.append(fn)

                            plt.figure(metric)
                            plt.plot(false_pos,true_pos,'x', color='black')
                            print(metric)
                            try:
                                #print(true_pos.index(109))
                                print(true_pos.index(125))
                            except:
                                pass

                            plt.ylabel(labely)
                            # plt.ylim(0,200)
                            # plt.yticks(list(np.arange(0,250,50)))

                            plt.xlabel(labelx)
                            # plt.xlim(0,3)
                            # plt.xticks(list(np.arange(0,4,1)))

                            plt.grid()


   # plt.legend(loc='upper right')

    #plt.grid()

    plt.show()

    

 

if __name__ == '__main__':
    main()