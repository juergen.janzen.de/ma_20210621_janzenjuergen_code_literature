import matplotlib.pyplot as plt
import os
import json
from numpy import mean, arange
import numpy as np
import itertools
from operator import itemgetter

colour= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1']
ylabel = 'Loss'
xlabel = ['Rot270', 'With Augmentations']#['12.5 %', '25 %', '50 %']#['25 %', '50 %', '75 %']#
labelx = ['mit', 'ohne']
titel = None #'270° Rotation'
#xlabel='Edge length'#'Overlap of the cropping operation'

y_list=list()
for _ in range(len(labelx)):
    y_list.append([])

plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 14,
    'figure.figsize': np.array([10.8,5.4])/2.54,#[4.2,2.7],np.array([13.8, 9.8]) / 2.54,#
    'figure.frameon':False,
    'figure.constrained_layout.use':True
})


os.system('cls')
avg_sampelsize = 20
legend = True
time = False
jason_file = "meter.json"

parent_folder = "D:\Studium\Master_RWTH\msrcnn\models\compendium_10022021"#"/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/compendium_10022021"
croppings = [
    # 'crop90_89',
    # 'crop110_114',
    # 'crop212_204',
    # 'crop300_92',
    # 'crop340_244',
    # "crop0300",
    # "crop0350",
    # "crop0600",
    # "crop0650",
    #"crop1200",
    #"crop1212",
    "crop1225",
    "crop1250",
    "crop1275",
    #'crop2500',
    "crop2525",
    "crop2550",
    "crop2575",
    #"crop5000",
    "crop5025",
    "crop5050",
    "crop5075"
]
defaults =[
    # "Blurr3",
    # "Blurr7",
    # "Blurr11",
    "FlipHori",
    "Rot90",
    "Rot270",
    "Rot180", 
    "FlipVert",
    'None'
] 

argument = [
    'loss',
    # 'loss_maskiou',
    # 'loss_objectness',
    # 'loss_mask',
    # 'loss_rpn_box_reg',
    # 'loss_box_reg',
    # 'loss_classifier',
]

combination_length = [1,2,3,4,5,6]

combinations = []
for i in combination_length:
    for combi in list(itertools.combinations(defaults , i)):
        if 'None' in combi:
            to_append = ''
            for default in combi:
                to_append += default
            combinations.append(to_append)

for crops in sorted(os.listdir(parent_folder), reverse=True):
    if crops in croppings:
        mainset_path = os.path.join(parent_folder, crops)
        for sub in sorted(os.listdir(mainset_path), key=len):
            if sub in combinations:
                sub_path = os.path.join(mainset_path, sub)
                jason_path = os.path.join(sub_path, jason_file)

                if not os.path.exists(jason_path):
                    print(sub_path)
                    continue

                with open(jason_path) as json_file:
                    meter = json.load(json_file)

                for arg in argument:

                    y = meter[arg]
                    y = [mean(y[i:i+avg_sampelsize]) for i in range(0,len(y)-avg_sampelsize,1)]

                    if xlabel[0] in sub:
                        y_list[0].append(y[-1])
                    else:
                        y_list[1].append(y[-1])

                    # for i, over in enumerate(['25','50','75']):
                    #     if crops[-2:] == over:
                    #         y_list[i].append(y[-1])

                    # for i, over in enumerate(['12','25','50']):
                    #     if crops[-4:-2] == over:
                    #         y_list[i].append(y[-1])

                    


# for i, l in enumerate(xlabel):
#     for value in y_list[i]:
#         plt.plot(l, value, 'x', color=color[0])

plt.boxplot(y_list, labels=labelx, whis=1.5, medianprops=dict(color='black'))

# for i, (box, median_obj) in enumerate(zip(result['boxes'], result['medians'])):
#
#     mini = result['caps'][2*i].get_xydata()[0][1]
#     maxi = result['caps'][2*i+1].get_xydata()[0][1]
#     q1 = box.get_xydata()[0][1]
#     q3 = box.get_xydata()[2][1]
#     iqr = q3-q1
#     median = median_obj.get_xydata()[0][1]
#     skew = (q3-median) - (median - q1)
#     print('{0}: {1}/ {2}/ {3}/ max: {4}/ min {5}'.format(i,iqr,median, skew, maxi, mini))
# exit()

plt.ylabel(ylabel)
#plt.xlabel(xlabel)
if croppings[0][-4:-2] == '50':
    plt.ylim(0.32,0.48)
    plt.yticks(arange(0.32,0.5,0.02))
elif croppings[0][-4:-2] == '25':
    plt.ylim(0.45,0.75)
    plt.yticks(arange(0.45,0.8,0.05))
else:
    plt.ylim(0.3, 1.1)
    plt.yticks(arange(0.3, 1.2, 0.1))
plt.title(titel)
plt.savefig('D:\Studium\Master_RWTH\Masterarbeit\Bilder\Verteidigung\SVG\loss_aug.svg', transparent=True)
plt.show()