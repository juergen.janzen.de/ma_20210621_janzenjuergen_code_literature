#import torch
import os
import matplotlib.pyplot as plt
from operator import itemgetter
import json
import numpy as np
#from adjustText import adjust_text

def make_save_figure(save_path, file_name, titel, xlabel, ylabel, x_list, y_list):
    plt.figure(file_name, figsize=[6, 4])
    plt.title(titel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x_list, y_list, marker='.')
    plt.xlim(left=0, right=max(x_list))
    plt.ylim(bottom=0, top=1)
    plt.grid()
    plt.savefig(os.path.join(save_path, file_name))
    plt.close()

def roc_costum(metric_list, num_gt):
    score_range = list(np.arange(0,1.1,0.1).astype(np.float16))
    precision_list = [None] * len(score_range)
    recall_list = [None] * len(score_range)
    for i, score in enumerate(score_range):
        new_metric_list = [value_dict for value_dict in metric_list if value_dict['score']>=score]
        if len(new_metric_list) > 0:
            precision_list[i], recall_list[i] = avg_prec_rec(new_metric_list, len(new_metric_list), num_gt)

        else:
            precision_list.remove(None)
            recall_list.remove(None)
            score_range.remove(score)

    return precision_list, recall_list, score_range

def konfusion_matrix(metric_list, num_gt, score_threshold, metric_threshold):
    actual_metric_list = [metric for metric in metric_list if metric['score'] >= score_threshold]
    metric_key = metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    tp = 0
    fp = 0
    for metric in actual_metric_list:
        if metric[metric_key] >= metric_threshold:
            tp += 1
        else:
            fp += 1

    fn = num_gt - tp

    return tp, fp, fn

def avg_prec_rec(metric_list, num_d, num_gt, metric_thresholds=None, invert=False):
    if metric_thresholds is None:
        metric_thresholds = np.arange(0.5,1,0.05)

    recall_values = np.arange(0,1.01,0.01)

    metric_list.sort(key=itemgetter('score'), reverse=True)
    metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    assert len(metric_list) == num_d, '{0}, {1}'.format(len(metric_list), num_d)
    prec_list = list()
    rec_list = list()
    for threshold in metric_thresholds:
        temp_dict = dict()
        temp_dict['precision'] = list()
        temp_dict['recall'] = list()
        tp = 0
        for i, detection in enumerate(metric_list):
            if not invert:
                if detection[metric_key] >= threshold:
                    tp += 1
            else:
                if detection[metric_key] <= threshold:
                    tp += 1
            assert tp <= num_gt
            temp_dict['precision'].append(tp/(i+1))
            temp_dict['recall'].append(tp/num_gt)
        
        precision = 0
        for interpol in recall_values:
            to_consider = [temp_dict['precision'][i] for i,rec in enumerate(temp_dict['recall']) if rec>=interpol]
            if len(to_consider) > 0:
                precision += max(to_consider)

        prec_list.append(precision/len(recall_values))
        rec_list.append(max(temp_dict['recall']))

    avg_prec = sum(prec_list)/len(metric_thresholds)
    avg_rec = sum(rec_list)/len(metric_thresholds)

    return avg_prec, avg_rec

def F_score(beta, precision, recall):
    if precision == 0 and recall == 0:
        score = 0
    else:
        score = (1+beta**2) * (precision*recall) / (beta**2 * precision + recall)
    return score    

def main():

    color= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1', '#000000', '#E30066', '#7A6fac', '#bdcd00']
    labely = 'F1-score using CoG'
    labelx = 'F1-score using F1-score'

    learning_lib = dict()

    plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [4.2,2.7],#[4.2,2.7],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
    })

    os.system('cls')
    org_path = "D:\Studium\Master_RWTH\msrcnn/validations/Crop2575_LR"
    dataset = "coco_val_insect"
    metirc_file = "eval_metric.json"
    make_fig = False
    beta = 1
    top = {
        'IOU': list(),
        'F1':list(),
        'COG':list()
    }
    label = None
    for LR in range(1,11):
        path = org_path + '{0}'.format(LR)

        for crops in sorted(os.listdir(path), reverse=False):
            crop_path = os.path.join(path,crops)

            for combi in sorted(os.listdir(crop_path), key=len):

                if combi not in learning_lib.keys():
                    learning_lib[combi]=dict()

                if LR not in learning_lib[combi].keys():
                    learning_lib[combi][LR]={
                        'F1':{
                            'F1':list(),
                            'mAP':list(),
                            'mAR':list()
                        },
                        'COG':{
                            'F1':list(),
                            'mAP':list(),
                            'mAR':list()
                        }
                    }

                combi_path = os.path.join(crop_path, combi, dataset)
                json_file = os.path.join(combi_path, metirc_file)
                
                with open(json_file) as f:
                    metric_dict = json.load(f)

                for metric in metric_dict:
                    if metric == 'IOG':
                        continue

                    if metric == 'COG':
                        threshold = [20000] #list(range(0,33,3))
                        invert = True
                    else:
                        threshold = np.arange(0.5,1,0.05)
                        invert = False

                    iteration = list()
                    f1_plot = list()
                    prec_plot = list()
                    rec_plot = list()

                    for it, it_dict in sorted(metric_dict[metric].items(), key=lambda item: int(item[0])):
                        iteration.append(int(it))
                        metric_subdict = dict()

                        for _, img_dict in it_dict.items():

                            for cat, cat_dict in img_dict.items():
                                
                                if cat in metric_subdict.keys():
                                    pass
                                else:
                                    metric_subdict[cat] = dict()
                                    metric_subdict[cat]['num_gt'] = 0
                                    metric_subdict[cat]['result'] = list()

                                metric_subdict[cat]['result'].extend(cat_dict['result'])
                                metric_subdict[cat]['num_gt'] += cat_dict['num_gt']

                        f1_score = list()
                        recall_score = list()
                        precision_score = list()
                        for cat, metric_list in metric_subdict.items():
                            avg_prec, avg_rec = avg_prec_rec(metric_list['result'], len(metric_list['result']), metric_list['num_gt'], threshold, invert)
                            f1 = F_score(beta, avg_prec, avg_rec)

                            f1_score.append(f1)
                            recall_score.append(avg_rec)
                            precision_score.append(avg_prec)

                        f1_plot.append(np.mean(f1_score))
                        rec_plot.append(np.mean(recall_score))
                        prec_plot.append(np.mean(precision_score))


                        if make_fig:

                            if metric == 'F1':
                                plt.plot(iteration, f1_plot)
                                plt.show()                       
                                plt.plot(iteration, prec_plot)
                                plt.plot(iteration, rec_plot)


                    if metric in learning_lib[combi][LR].keys():
                        learning_lib[combi][LR][metric]['F1'] = f1_plot
                        learning_lib[combi][LR][metric]['mAP'] = prec_plot
                        learning_lib[combi][LR][metric]['mAR'] = rec_plot

    for t, (combo, combo_item) in enumerate(learning_lib.items()):

        plt.figure(combo)
        if combo != 'Rot180FlipVertNone':
            plt.title('Dataset {0}'.format(t+1))
        rate_old=0
        for rate, item in combo_item.items():

            choose = [(combo == 'None' and rate == 10), ('Rot180FlipVert' in combo and rate == 9), ('Rot90Rot270F' in combo and rate == 7), ('FlipHori' in combo and rate == 10)]

            if True in choose:
                print('\033[1m\n{0}\033[0m'.format(combo))

                if choose[0] or choose[1]:
                    if choose[0]:
                        thresh = 0.75
                    else:
                        thresh = 0.74

                    for j, ele in enumerate(item['COG']['F1']):
                        if ele > thresh:
                            iter_max = j

                if choose[2] or choose[3]:
                    if choose[2]:
                        thresh = 0.43
                    else:
                        thresh = 0.44

                    for j, ele in enumerate(item['F1']['F1']):
                        if ele > thresh:
                            iter_max = j

                print('\n    Iteration {0}:'.format(iter_max+1))
                print('\n      F1-score:')
                print('        F1: {0:.3f}'.format(item['F1']['F1'][iter_max]))
                print('        mAP: {0:.3f}'.format(item['F1']['mAP'][iter_max]))
                print('        mAR: {0:.3f}'.format(item['F1']['mAR'][iter_max]))
                print('\n      COG:')
                print('        F1: {0:.3f}'.format(item['COG']['F1'][iter_max]))
                print('        mAP: {0:.3f}'.format(item['COG']['mAP'][iter_max]))
                print('        mAR: {0:.3f}'.format(item['COG']['mAR'][iter_max]))

            colour = 'black'#color[int(rate)-1]
            # if (max(item['F1']['F1']) > 0.4) and (max(item['COG']['F1']) > 0.7):
            #     label = '{0}'.format(rate/10000)
            # else:
            #     label=None

            if rate == rate_old:
                label = None
            else:
                label = '{0}'.format(rate / 10000)
                rate_old = rate

            plt.plot(item['F1']['F1'], item['COG']['F1'], 'x', color=colour, label=label)

        
        plt.ylabel(labely)
        plt.ylim(0,0.8)#plt.ylim(0.7,0.8)
        plt.yticks(list(np.arange(0,0.9,0.1)))#plt.yticks(list(np.arange(0.7,0.8,0.01)))

        plt.xlabel(labelx)
        plt.xlim(0,0.5)
        plt.xticks(list(np.arange(0,0.6,0.1)))
        # plt.xlim(0.4, 0.5)
        # plt.xticks(list(np.arange(0.4, 0.51, 0.01)))


        #plt.legend(loc='upper left', bbox_to_anchor=(1,1.03))

        plt.grid()

    plt.show()

    

 

if __name__ == '__main__':
    main()