import os
import matplotlib.pyplot as plt
from operator import itemgetter
import json
import numpy as np
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

def make_save_figure(save_path, file_name, titel, xlabel, ylabel, x_list, y_list):
    plt.figure(file_name, figsize=[6, 4])
    plt.title(titel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x_list, y_list, marker='.')
    plt.xlim(left=0, right=max(x_list))
    plt.ylim(bottom=0, top=1)
    plt.grid()
    plt.savefig(os.path.join(save_path, file_name))
    plt.close()

def roc_costum(metric_list, num_gt):
    score_range = list(np.arange(0,1.1,0.1).astype(np.float16))
    precision_list = [None] * len(score_range)
    recall_list = [None] * len(score_range)
    for i, score in enumerate(score_range):
        new_metric_list = [value_dict for value_dict in metric_list if value_dict['score']>=score]
        if len(new_metric_list) > 0:
            precision_list[i], recall_list[i] = avg_prec_rec(new_metric_list, len(new_metric_list), num_gt)

        else:
            precision_list.remove(None)
            recall_list.remove(None)
            score_range.remove(score)

    return precision_list, recall_list, score_range

def konfusion_matrix(metric_list, num_gt, score_threshold, metric_threshold):
    actual_metric_list = [metric for metric in metric_list if metric['score'] >= score_threshold]
    metric_key = metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    tp = 0
    fp = 0
    for metric in actual_metric_list:
        if metric[metric_key] >= metric_threshold:
            tp += 1
        else:
            fp += 1

    fn = num_gt - tp

    return tp, fp, fn

def avg_prec_rec(metric_list, num_d, num_gt, metric_thresholds=None, invert=False):
    if metric_thresholds is None:
        metric_thresholds = np.arange(0.5,1,0.05)

    recall_values = np.arange(0,1.01,0.01)

    metric_list.sort(key=itemgetter('score'), reverse=True)
    metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    assert len(metric_list) == num_d, '{0}, {1}'.format(len(metric_list), num_d)
    prec_list = list()
    rec_list = list()
    for threshold in metric_thresholds:
        temp_dict = dict()
        temp_dict['precision'] = list()
        temp_dict['recall'] = list()
        tp = 0
        for i, detection in enumerate(metric_list):
            if not invert:
                if detection[metric_key] >= threshold:
                    tp += 1
            else:
                if detection[metric_key] <= threshold:
                    tp += 1
            assert tp <= num_gt
            temp_dict['precision'].append(tp/(i+1))
            temp_dict['recall'].append(tp/num_gt)
        
        precision = 0
        for interpol in recall_values:
            to_consider = [temp_dict['precision'][i] for i,rec in enumerate(temp_dict['recall']) if rec>=interpol]
            if len(to_consider) > 0:
                precision += max(to_consider)

        prec_list.append(precision/len(recall_values))
        rec_list.append(max(temp_dict['recall']))

    avg_prec = sum(prec_list)/len(metric_thresholds)
    avg_rec = sum(rec_list)/len(metric_thresholds)

    return avg_prec, avg_rec

def F_score(beta, precision, recall):
    if precision == 0 and recall == 0:
        score = 0
    else:
        score = (1+beta**2) * (precision*recall) / (beta**2 * precision + recall)
    return score    

def main():

    color= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1']
    labelx = 'F1-score using F1-score'
    labely = 'F1-score using CoG'

    learning_lib = dict()

    plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [4.2,2.7],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
    })

    os.system('cls')
    path = "D:\Studium\Master_RWTH\msrcnn/validations/Resolution" #Rot180FlipVert_BSF" #"/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/validations/compendium_5000it"
    #"/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/validations/compendium_5000it"
    dataset_list = ["coco_test_insect", "coco_test_res60", "coco_test_res40"] #"coco_test_fly"
    metirc_file = "eval_metric.json"
    make_fig = False
    beta = 1
    top = {
        'IOU': list(),
        'F1':list(),
        'COG':list()
    }

    for crops in sorted(os.listdir(path), reverse=False):
        crop_path = os.path.join(path,crops)

        for j, dataset in enumerate(dataset_list):

            for combi in sorted(os.listdir(crop_path), key=len):

                if combi == '100':
                    it_int = '5000'
                    label = '100% Resolution'
                    symbol = 'x'
                elif combi == '80':
                    it_int = '3000'
                    label = '80% Resolution'
                    symbol = '.'
                else:
                    it_int = '9000'
                    label = '40% Resolution'
                    symbol = '^'

                if combi not in learning_lib.keys():
                    learning_lib[combi]={
                        'F1':{
                            'F1':list(),
                            'mAP':list(),
                            'mAR':list()
                        },
                        'COG':{
                            'F1':list(),
                            'mAP':list(),
                            'mAR':list()
                        }
                    }

                combi_path = os.path.join(crop_path, combi, dataset)
                json_file = os.path.join(combi_path, metirc_file)

                with open(json_file) as f:
                    metric_dict = json.load(f)

                for metric in metric_dict:
                    if metric == 'IOG':
                        continue

                    if metric == 'COG':
                        threshold = [20000] #list(range(0,33,3))
                        invert = True
                    else:
                        threshold = np.arange(0.5,1,0.05)
                        invert = False

                    iteration = list()
                    f1_plot = list()
                    prec_plot = list()
                    rec_plot = list()

                    for it, it_dict in sorted(metric_dict[metric].items(), key=lambda item: int(item[0])):

                        if it != it_int:
                            continue

                        iteration.append(int(it))
                        metric_subdict = dict()

                        for _, img_dict in it_dict.items():

                            for cat, cat_dict in img_dict.items():

                                if cat in metric_subdict.keys():
                                    pass
                                else:
                                    metric_subdict[cat] = dict()
                                    metric_subdict[cat]['num_gt'] = 0
                                    metric_subdict[cat]['result'] = list()

                                metric_subdict[cat]['result'].extend(cat_dict['result'])
                                metric_subdict[cat]['num_gt'] += cat_dict['num_gt']

                        f1_score = list()
                        recall_score = list()
                        precision_score = list()
                        for cat, metric_list in metric_subdict.items():
                            if len(metric_list['result']) == 0:
                                avg_prec = 0
                                avg_rec = 0
                                f1 = 0
                            else:
                                avg_prec, avg_rec = avg_prec_rec(metric_list['result'], len(metric_list['result']), metric_list['num_gt'], threshold, invert)
                                f1 = F_score(beta, avg_prec, avg_rec)

                            f1_score.append(f1)
                            recall_score.append(avg_rec)
                            precision_score.append(avg_prec)

                        #     if it == '1000':
                        #         print(cat)
                        #
                        # if it == '1000':
                        #     print(metric)
                        #     print(f1_score)
                        f1_plot.append(np.mean(f1_score))
                        rec_plot.append(np.mean(recall_score))
                        prec_plot.append(np.mean(precision_score))


                    if metric in learning_lib[combi].keys():
                        learning_lib[combi][metric]['F1'] = f1_plot
                        learning_lib[combi][metric]['mAP'] = prec_plot
                        learning_lib[combi][metric]['mAR'] = rec_plot

            for combo, item in learning_lib.items():

                if combo == '100':
                    label = '100% Resolution'
                    symbol = 'x'
                elif combo == '80':
                    label = '80% Resolution'
                    symbol = '.'
                else:
                    label = '40% Resolution'
                    symbol = '^'

                for i, f in enumerate(item['F1']['F1']):
                    if f > 0.005:
                        print('{0:.3f}/{1:.3f}/{2:.3f}'.format(item['F1']['F1'][i], item['F1']['mAP'][i], item['F1']['mAR'][i]))
                        print('{0:.3f}/{1:.3f}/{2:.3f}'.format(item['COG']['F1'][i], item['COG']['mAP'][i], item['COG']['mAR'][i]))
                        print(i+1)

                plt.plot(item['F1']['F1'], item['COG']['F1'], symbol, color=color[j])

    plt.ylabel(labely)
    plt.ylim(0,0.8)
    plt.yticks(list(np.arange(0,0.9,0.1)))
    #plt.title('100% Resolution')
    plt.xlabel(labelx)
    plt.xlim(0,0.5)
    plt.xticks(list(np.arange(0,0.6,0.1)))

    legend_elements = [ Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label=''),
                        Patch(color='none', label='40 %'),
                        Patch(color='none', label='80 %'),
                        Patch(color='none', label='100 %'),
                        Patch(color='none', label=''),
                        Patch(color='none', label='40 %'),
                        Line2D([0], [0], lw=0, marker='^', color=color[2]),
                        Line2D([0], [0], lw=0, marker='^', color=color[1]),
                        Line2D([0], [0], lw=0, marker='^', color=color[0]),
                        Patch(color='none', label=''),
                        Patch(color='none', label='80 %'),
                        Line2D([0], [0], lw=0, marker='.', color=color[2]),
                        Line2D([0], [0], lw=0, marker='.', color=color[1]),
                        Line2D([0], [0], lw=0, marker='.', color=color[0]),
                        Patch(color='none', label=''),
                        Patch(color='none', label='100 %'),
                        Line2D([0], [0], lw=0, marker='x', color=color[2]),
                        Line2D([0], [0], lw=0, marker='x', color=color[1]),
                        Line2D([0], [0], lw=0, marker='x', color=color[0]),
                       ]
    plt.legend(handles=legend_elements, loc='lower right', ncol=5, handletextpad=-2.5, borderpad=0.8)
    plt.text(0.33, 0.35, 'trained on',zorder=10)
    plt.text(0.165, 0.08, 'tested on', zorder=10, rotation=90)

    plt.grid()

    plt.show()

    

 

if __name__ == '__main__':
    main()