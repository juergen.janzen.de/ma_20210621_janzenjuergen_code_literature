import torch
import os
import matplotlib.pyplot as plt
from operator import itemgetter
import json
import numpy as np
from adjustText import adjust_text

def make_save_figure(save_path, file_name, titel, xlabel, ylabel, x_list, y_list):
    plt.figure(file_name, figsize=[6, 4])
    plt.title(titel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x_list, y_list, marker='.')
    plt.xlim(left=0, right=max(x_list))
    plt.ylim(bottom=0, top=1)
    plt.grid()
    plt.savefig(os.path.join(save_path, file_name))
    plt.close()

def roc_costum(metric_list, num_gt):
    score_range = list(np.arange(0,1.1,0.1).astype(np.float16))
    precision_list = [None] * len(score_range)
    recall_list = [None] * len(score_range)
    for i, score in enumerate(score_range):
        new_metric_list = [value_dict for value_dict in metric_list if value_dict['score']>=score]
        if len(new_metric_list) > 0:
            precision_list[i], recall_list[i] = avg_prec_rec(new_metric_list, len(new_metric_list), num_gt)

        else:
            precision_list.remove(None)
            recall_list.remove(None)
            score_range.remove(score)

    return precision_list, recall_list, score_range

def konfusion_matrix(metric_list, num_gt, score_threshold, metric_threshold):
    actual_metric_list = [metric for metric in metric_list if metric['score'] >= score_threshold]
    metric_key = metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    tp = 0
    fp = 0
    for metric in actual_metric_list:
        if metric[metric_key] >= metric_threshold:
            tp += 1
        else:
            fp += 1

    fn = num_gt - tp

    return tp, fp, fn

def avg_prec_rec(metric_list, num_d, num_gt, metric_thresholds=None, invert=False):
    if metric_thresholds is None:
        metric_thresholds = np.arange(0.5,1,0.05)

    recall_values = np.arange(0,1.01,0.01)

    metric_list.sort(key=itemgetter('score'), reverse=True)
    metric_key = [k for k in metric_list[0].keys() if k != 'score'][0]
    assert len(metric_list) == num_d, '{0}, {1}'.format(len(metric_list), num_d)
    prec_list = list()
    rec_list = list()
    for threshold in metric_thresholds:
        temp_dict = dict()
        temp_dict['precision'] = list()
        temp_dict['recall'] = list()
        tp = 0
        for i, detection in enumerate(metric_list):
            if not invert:
                if detection[metric_key] >= threshold:
                    tp += 1
            else:
                if detection[metric_key] <= threshold:
                    tp += 1
            assert tp <= num_gt
            temp_dict['precision'].append(tp/(i+1))
            temp_dict['recall'].append(tp/num_gt)
        
        precision = 0
        for interpol in recall_values:
            to_consider = [temp_dict['precision'][i] for i,rec in enumerate(temp_dict['recall']) if rec>=interpol]
            if len(to_consider) > 0:
                precision += max(to_consider)

        prec_list.append(precision/len(recall_values))
        rec_list.append(max(temp_dict['recall']))

    avg_prec = sum(prec_list)/len(metric_thresholds)
    avg_rec = sum(rec_list)/len(metric_thresholds)

    return avg_prec, avg_rec

def F_score(beta, precision, recall):
    if precision == 0 and recall == 0:
        score = 0
    else:
        score = (1+beta**2) * (precision*recall) / (beta**2 * precision + recall)
    return score    

def main():

    color= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1']
    ylabel = 'F1-score'
    xlabel = 'Iteration'

    learning_lib = dict()

    plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [6.4,4],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
    })

    os.system('clear')
    path = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/validations/Rot180FlipVert_BSF"
    dataset = "coco_test_fly"
    metirc_file = "eval_metric.json"
    save_path_org = "/home/jjuergen/Desktop/Eval_Models_Crop2575"#_1275_LR1"
    #spec_crop = ['crop1225', 'crop1250', 'crop1275']
    spec_combi = ['None']
    make_fig = True
    beta = 1
    top = {
        'IOU': list(),
        'F1':list(),
        'COG':list()
    }

    for crops in os.listdir(path):
        # if crops not in spec_crop:
        #     continue
        crop_path = os.path.join(path,crops)

        for combi in sorted(os.listdir(crop_path), key=len):
            # if combi not in spec_combi:
            #     continue

            if combi not in learning_lib.keys():
                learning_lib[combi]={
                    'F1':{
                        'F1':list(),
                        'mAP':list(),
                        'mAR':list()
                    },
                    'COG':{
                        'F1':list(),
                        'mAP':list(),
                        'mAR':list()
                    }
                }

            combi_path = os.path.join(crop_path, combi, dataset)
            json_file = os.path.join(combi_path, metirc_file)
            save_path = os.path.join(save_path_org, crops, combi)

            if not os.path.exists(save_path) and make_fig:
                os.makedirs(save_path)
            
            with open(json_file) as f:
                metric_dict = json.load(f)

            for metric in metric_dict:
                if metric == 'IOG':
                    continue

                if metric == 'COG':
                    threshold = [20000] #list(range(0,33,3))
                    invert = True
                else:
                    threshold = np.arange(0.5,1,0.05)
                    invert = False

                iteration = list()
                f1_plot = list()
                prec_plot = list()
                rec_plot = list()

                for it, it_dict in sorted(metric_dict[metric].items(), key=lambda item: int(item[0])):
                    iteration.append(int(it))
                    metric_subdict = dict()

                    for _, img_dict in it_dict.items():

                        for cat, cat_dict in img_dict.items():
                            
                            if cat in metric_subdict.keys():
                                pass
                            else:
                                metric_subdict[cat] = dict()
                                metric_subdict[cat]['num_gt'] = 0
                                metric_subdict[cat]['result'] = list()

                            metric_subdict[cat]['result'].extend(cat_dict['result'])
                            metric_subdict[cat]['num_gt'] += cat_dict['num_gt']

                    f1_score = list()
                    recall_score = list()
                    precision_score = list()
                    for cat, metric_list in metric_subdict.items():
                        if len(metric_list['result']) == 0:
                            avg_prec = 0
                            avg_rec = 0
                            f1 = 0
                        else:
                            avg_prec, avg_rec = avg_prec_rec(metric_list['result'], len(metric_list['result']), metric_list['num_gt'], threshold, invert)
                            f1 = F_score(beta, avg_prec, avg_rec)

                        f1_score.append(f1)
                        recall_score.append(avg_rec)
                        precision_score.append(avg_prec)

                    f1_plot.append(np.mean(f1_score))
                    rec_plot.append(np.mean(recall_score))
                    prec_plot.append(np.mean(precision_score))

                if metric in learning_lib[combi].keys():
                    learning_lib[combi][metric]['F1'] = f1_plot
                    learning_lib[combi][metric]['mAP'] = prec_plot
                    learning_lib[combi][metric]['mAR'] = rec_plot

                # if metric not 'IOU':
                #     f1_max = max(f1_plot)
                #     f1_idx = f1_plot.index(f1_max)
                #     item = (f1_max, prec_plot[f1_idx], rec_plot[f1_idx], crops, combi, f1_idx+1)
                #     top[metric].append(item)
                #     top[metric] = sorted(top[metric], key=itemgetter(0), reverse=True)


    if make_fig:
        for combo, combo_item in learning_lib.items():
            for meter, item in combo_item.items():
                plt.figure('{0}_{1}'.format(combo, meter))
                plt.plot(iteration, item['F1'], color=color[0])
                        
                plt.ylabel(ylabel)
                plt.ylim(0,1)
                plt.yticks(list(np.arange(0,1.1,0.1)))

                plt.xlabel(xlabel)
                plt.xlim(0,10000)
                plt.xticks(list(np.arange(0,11000,1000)))

                plt.grid()
            
            plt.show()                       

    
    # for m,score_chart in top.items():
    #     print('\n{0}:'.format(m))
    #     print('Top 5:')
    #     for i in score_chart[:5]:
    #         print('{0}/ {1} at {2}: mF1({3}), mAP({4}), mAR({5})'.format(i[3],i[4],i[5],i[0],i[1],i[2]))

        # print('\nLeast 5:')
        # for i in score_chart[-5:]:
        #     print('{0}/ {1} at {2}: mF1({3}), mAP({4}), mAR({5})'.format(i[3],i[4],i[5],i[0],i[1],i[2]))

    # for combo, item in sorted(learning_lib.items(), key= lambda x:len(x[0])):
    #     print('\033[1m\n{0}\033[0m'.format(combo))
    #     f1_f1_idx = item['F1']['F1'].index(max(item['F1']['F1']))
    #     cog_f1_idx = item['COG']['F1'].index(max(item['COG']['F1']))

    #     if f1_f1_idx == cog_f1_idx:
    #         iter_max = [f1_f1_idx]
    #     else:
    #         iter_max = sorted([f1_f1_idx, cog_f1_idx])

    #     for i in iter_max:
    #         print('\n    Iteration {0}:'.format(i+1))
    #         print('\n      F1-score:')
    #         print('        F1: {0:.3f}'.format(item['F1']['F1'][i]))
    #         print('        mAP: {0:.3f}'.format(item['F1']['mAP'][i]))
    #         print('        mAR: {0:.3f}'.format(item['F1']['mAR'][i]))
    #         print('\n      COG:')
    #         print('        F1: {0:.3f}'.format(item['COG']['F1'][i]))
    #         print('        mAP: {0:.3f}'.format(item['COG']['mAP'][i]))
    #         print('        mAR: {0:.3f}'.format(item['COG']['mAR'][i]))

    

 

if __name__ == '__main__':
    main()