import matplotlib.pyplot as plt
import os
import json
from numpy import mean, arange
import itertools
from operator import itemgetter

color= ['#00549f', '#57ab27', '#cc071e', '#f6a800', '#612158', '#0098a1']
ylabel = 'Summed loss'
xlabel = 'Iterations'
#labels = ['crop size 12.5%', 'crop size 25%', 'crop size 50%']
labels = ['overlap 25%', 'overlap 50%', 'overlap 75%', 'augmented baseline']
# labels = {
#     'loss_maskiou': 'mask IoU',
#     'loss_objectness': 'objectness',
#     'loss_mask': 'mask',
#     'loss_rpn_box_reg': 'RPN regression',
#     'loss_box_reg': 'bounding box regression',
#     'loss_classifier': 'classification',
# }

plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [6.4,4],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
})

#fig, axs = plt.subplots(3,1)

os.system('clear')
avg = True
avg_sampelsize = 20
legend = True
time = False
jason_file = "meter.json"

#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/compendium_10022021"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/None5000It"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Benchmark_01032021"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Benchmark_01032021_LR_schedual"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Benchmark_01032021_LR_schedual_2"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Benchmark_01032021_LR_schedual_06"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/BestCombo"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/BestCombo_LR_06_01"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/CropCombi"
#parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Crop2575"
parent_folder = "/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Rot180FlipVert_allinsects"

croppings = [
    # 'crop90_89',
    # 'crop110_114',
    # 'crop212_204',
    # 'crop300_92',
    # 'crop340_244',
    # "crop0300",
    # "crop0350",
    # "crop0600",
    # "crop0650",
    #"crop1200",
    #"crop1212",
    # "crop1225",
    # "crop1250",
    #'crop2500',
    # "crop2525",
    # "crop2550",
    "crop2575",
    #"crop5000",
    #"crop5025",
    #"crop5050",
    #"crop5075"
]
defaults =[
    # "Blurr3",
    # "Blurr7",
    # "Blurr11",
    #"FlipHori",
    #"Rot90",
    #"Rot270",
    "Rot180", 
    "FlipVert",
    'None'
] 

argument = [
    #'loss',
    # 'loss_maskiou',
    # 'loss_objectness',
    # 'loss_mask',
    # 'loss_rpn_box_reg',
    # 'loss_box_reg',
    # 'loss_classifier',
]

combination_length = [1,2,3,4,5,6]

combinations = []
for i in combination_length:
    for combi in list(itertools.combinations(defaults , i)):
        if 'None' in combi:
            to_append = ''
            for default in combi:
                to_append += default
            combinations.append(to_append)

# plt.rcParams['font.size'] = '10'
# if not multi_fig:
#     num_colors = len(croppings)*len(argument)*len(combinations)
#     cm = plt.get_cmap('gist_rainbow') 
#     titel = str(argument) + " " + str(croppings)           
#     fig = plt.figure(titel, figsize=[6, 4] )
#     ax = fig.add_subplot(111)
#     ax.set_prop_cycle(color=[cm(i/num_colors) for i in range(num_colors)])

count=0
top5 = list()
least5 = list()
none = list()

for crops in sorted(os.listdir(parent_folder), reverse=True):
    if crops in croppings:
        mainset_path = os.path.join(parent_folder, crops)
        for sub in sorted(os.listdir(mainset_path), key=len):
            if sub in combinations:
                sub_path = os.path.join(mainset_path, sub)
                jason_path = os.path.join(sub_path, jason_file)

                if not os.path.exists(jason_path):
                    print(sub_path)
                    continue

                with open(jason_path) as json_file:
                    meter = json.load(json_file)

                for arg in argument:

                    y = meter[arg]

                    if avg:
                        y = [mean(y[i:i+avg_sampelsize]) for i in range(0,len(y)-avg_sampelsize,1)]
                        #y = y[:5000-avg_sampelsize]            

                    if time:
                        x = [sum(meter['iteration_time'][:i]) - sum(meter['data'][:i]) for i in range(len(y))]
                    else:
                        x = list(range(len(y)))

                    label = crops + ': ' + sub

                    # for i,(k,l) in enumerate(labels.items()):
                    #     if k == arg:
                    #         plt.plot(x,y, label=l, color=color[i])
                    #         break

                    # if sub == 'None':

                    #     if crops[-2:] == '25':
                    #         plt.plot(x,y, label=labels[0], color=color[0], zorder=-1)
                        
                    #     if crops[-2:] == '50':
                    #         plt.plot(x,y, label=labels[1], color=color[0], zorder=-1)

                    #     if crops[-2:] == '75':
                    #         plt.plot(x,y, label=labels[2], color=color[0], zorder=-1)

                    if 'Rot90' in sub:
                        plt.plot(x,y, color=color[1], zorder=1)

                    else:
                        plt.plot(x,y, color=color[0],  zorder=-1)


                    # if crops in ['crop1225', 'crop1250', 'crop1275']:
                    #     if crops == 'crop1225' and sub == 'None':
                    #         plt.plot(x,y, label=labels[0], color=color[0])
                    #     else:
                    #         plt.plot(x,y, color=color[0])
                    
                    # elif crops in ['crop2525', 'crop2550', 'crop2575']:
                    #     if crops == 'crop2525' and sub == 'None':
                    #         plt.plot(x,y, label=labels[1], color=color[1])
                    #     else:
                    #         plt.plot(x,y, color=color[1])
                    
                    # elif crops in ['crop5025', 'crop5050', 'crop5075']:
                    #     if crops == 'crop5025' and sub == 'None':
                    #         plt.plot(x,y, label=labels[2], color=color[2])
                    #     else:
                    #         plt.plot(x,y, color=color[2])

                    if sub == 'None':
                        none.append([arg, label, y[len(y)-1]])

                    top5.append([label, y[len(y)-1]])
                    least5.append([label, y[len(y)-1]])
                    top5 = sorted(top5, key=itemgetter(1))
                    least5 = sorted(least5, key=itemgetter(1))
                    if len(top5) > 5:
                        top5 = top5[0:5]
                        least5 = least5[1:]
print('None:')
for loss_type in none:
    print(loss_type)

print('top5:')
for top in top5:
    print(top)

print('least5:')
for least in least5:
    print(least)

if legend:
    plt.legend(bbox_to_anchor=(1, 1), loc='upper right')


plt.xlabel(xlabel)
plt.xlim(0,10000)
plt.xticks(arange(0,11000,1000))

plt.ylabel(ylabel)
plt.ylim(0,3)
plt.yticks(arange(0,3.5,0.5))
plt.grid()
plt.show()