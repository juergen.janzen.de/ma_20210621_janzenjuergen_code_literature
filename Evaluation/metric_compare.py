import matplotlib.pyplot as plt
import numpy as np

color1 = '#00549f'
color2 = '#57AB27'

labely = 'Score value'
labelx = 'Fraction of the max. intersection'
titel = ['Smaller', 'Equal', 'Bigger']

plt.rcParams.update({
    'font.sans-serif':'Arial',
    'font.size': 10,
    'figure.figsize': [6.4,4],
    'figure.frameon':False,
    'figure.constrained_layout.use':True
})

A = 1
B = list(np.arange(0.8, 1.4, 0.2))

fig, axs = plt.subplots(1,3)

for n, (b, t) in enumerate(zip(B, titel)):
    intersection = list(np.arange(0,min(A,b)+0.1,0.1))
    IOU = list()
    F1 = list()
    F05 = list()
    IOG = list()
    for i in intersection:
        IOU.append(i/(A+b-i))
        F1.append(2*i/(A+b))
    
    intersection = [i/max(intersection) for i in intersection]
    axs[n].plot(intersection,IOU,label='IoU', color=color1)
    axs[n].plot(intersection,F1,label='F1', color=color2)
    axs[n].set_title(t, fontsize=10)
    axs[n].set_xlim([0,1])
    axs[n].set_yticks([1, 0.5])
    axs[n].set_ylim([0,1])
    axs[n].legend()
    axs[n].grid()

axs[0].set_ylabel(labely)
axs[1].set_xlabel(labelx)
plt.show()