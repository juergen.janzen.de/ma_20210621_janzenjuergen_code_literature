import cv2
import json
import os
from numpy import mean, std

img_folder = '/home/jjuergen/Desktop/masterarbeit/Bilder/Fliegenlarven/Original'
#jason = 'annotation.json'

# with open(os.path.join(img_folder, jason)) as inputfile:
#     train_jason = json.load(inputfile)

# img_files = [train_jason["images"][i]["file_name"] for i in range(len(train_jason["images"]))]
pixel_r = []
pixel_g = []
pixel_b = []
for img_file in os.listdir(img_folder):
    if 'json' in img_file:
        continue
    img = cv2.imread(os.path.join(img_folder, img_file))
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            r,g,b = img[i,j]
            pixel_r.append(r)
            pixel_g.append(g)
            pixel_b.append(b)

pixel_mean = [mean(pixel_r), mean(pixel_g), mean(pixel_b)]
pixel_std = [std(pixel_r), std(pixel_g), std(pixel_b)]

print(pixel_mean)
print(pixel_std)