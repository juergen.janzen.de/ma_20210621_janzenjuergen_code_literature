# Usage of the Mask Scoring R-CNN

## Installation and helpful links

Before Mask Scoring R-CNN (MS R-CNN) can be used, a few installations need to be done. Please refer to the `INSTALL.md` or the original [GitHub-Repositiory](https://github.com/zjhuang22/maskscoring_rcnn) by the authors of MS R-CNN. Furthermore, the blog [Programmers Help](https://programmer.help/blogs/training-your-own-dataset-with-mask-scoring-rcnn.html) gives usefull pointers on how to use MS R-CNN. However, the usage will be explained in the following.

## Usage
### Step1: Configuration

Before the algorithm can be run a few files need your attention. For training, testing and validation, the datasetpath need to be known. These paths are saved in the `.\maskrcnn_benchmark\config\path_catalog.py `. as an dictonary,wherein the key is a label for the config-file (explained later) and the corresponding tuple containes the path to the images and the annotations-file.

![](paths_catalog.jpg)

The config-files (abc.YAML) are found in `.\configs`. The structure,weigthts and training parameters can be adjusted with these files. All possible configurations and theire default values are observable, commented ,editable and expandable in `.\maskrcnn_benchmark\config\default.py `. The Parameter MODEL.WEIGHT defines where the weights are to be found for testing and validation and for training where the weights of the backbone are. Which Backbone is to be used and configuration of the Backbone structure ist possible with the parameters under BACKBONE.

![](model_config.jpg)

The parameter set DATASET (cataloged in path_catalog.py) defines which datasets are to be used for training and testing, while mutliple datasets can be combined. However, this set has been expanded for training seperatly on various different datasets. At this point, the parameters DATASET.COMBI and DATASET.EPOCHS have no meaning anymore but have not been deleted.

![](dataset_config.jpg)
![](dataset_config_rev.jpg)

### Step 2: Run Training and Testing

Run the following commands (just examples) for training and testing. For the master thesis the train script and test scrip has been modified in a way, that mutiple training runs and test runs are subsequently possible:

Original Training <br />
```
python tool/train_net.py --config-file configs/e2e_ms_rcnn_R_50_FPN_1x.yaml --skip-test
```

Custom Training <br />
```
python tool/train_net_custom.py --config-file configs/e2e_ms_rcnn_R_50_FPN_1x.yaml --skip-test
```

Original Testing <br />
```
python tool/test_net.py --config-file configs/e2e_ms_rcnn_R_50_FPN_1x.yaml
```

Custom Testing <br />
```
python tool/test_net_costum.py --config-file configs/e2e_ms_rcnn_R_50_FPN_1x.yaml
```

### Step 3: Run the trained MS R-CNN

First the config-file needs to be adjusted like the config-files for testing. Subsequently, additional configurations need to be done in the `.demo\predict.py`. Furthermore, the used classes for detection need to be edit in the `.demo\predictor.py`. Additional information can be extracted by using print statements at the end of the `predict.py`.

![](predictor_classes.jpg)

To run the prediction just use following command: 
```
python demo/predict.py
```