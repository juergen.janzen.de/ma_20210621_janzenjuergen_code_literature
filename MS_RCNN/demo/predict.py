#!/usr/bin/env python
# coding=UTF-8

import os, sys
import numpy as np
import cv2
from maskrcnn_benchmark.config import cfg
from predictor import COCODemo
from numpy import mean
import json

def insect_size(mask, image=None):
    num_annot, num_classes, mask_width, _ = mask.shape
    width = []
    length = []
    centroids = []
    for i in range(num_annot):
        x_cont = np.array([], int)
        y_cont = np.array([], int)
        for j in range(num_classes):
            y,x = np.where(mask[i][j]==1)
            cent = np.array([mean(x),mean(y)],int)
            centroids.append(list(cent))

            for line in range(min(y), max(y)+1):
                pixel = np.where(y==line)
                x_in_line = x[pixel]

                if len(x_in_line) == 0:
                    continue
                
                x_cont = np.append(x_cont,[min(x_in_line), max(x_in_line)])
                y_cont = np.append(y_cont, [line, line])

            dist_min = mask_width
            dist_max = 0
            for cont in zip(x_cont,y_cont):
                cont = np.array(cont)
                dist = np.linalg.norm(cent-cont)

                if dist < dist_min:
                    point_width = cont.astype(int)
                    dist_min = dist
                
                if dist > dist_max:
                    point_length = cont.astype(int)
                    dist_max = dist

            image = cv2.line(image, tuple(point_width), tuple(point_width+2*(cent-point_width)), color=(0,0,255), thickness=1)
            image = cv2.line(image, tuple(point_length), tuple(point_length+2*(cent-point_length)), color=(0,0,255), thickness=1)
            width.append(int(dist_min))
            length.append(int(dist_max))
    
    return width, length, centroids, image


# os.environ["CUDA_VISIBLE_DEVICES"] = '0'

# 1.Modified profile
config_file = "configs/e2e_ms_rcnn_R_101_FPN_1x_detection.yaml"

# 2.To configure
cfg.merge_from_file(config_file) # merge profile
cfg.merge_from_list(["MODEL.MASK_ON", True]) # Turn on the mask switch
cfg.merge_from_list(["MODEL.DEVICE", "cuda"]) # or Set to CPU ["MODEL.DEVICE", "cpu"]
#cfg.merge_from_list(["MODEL.DEVICE", "cpu"])

coco_demo = COCODemo(
    cfg,
    confidence_threshold=0.77, # 3.Set confidence
)

if __name__ == '__main__':

    in_folder = './datasets/testing_in/'
    out_folder = './datasets/testing_out/'

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    for file_name in os.listdir(in_folder):
        if not file_name.endswith(('jpg', 'png')):
            continue

        # load file
        img_path = os.path.join(in_folder, file_name)
        image = cv2.imread(img_path)

        # Method 1. Get opencv image results directly
        #predictions = coco_demo.run_on_opencv_image(image)
        #save_path = os.path.join(out_folder, file_name)
        #cv2.imwrite(save_path, predictions)

        # Method 2. Get forecast results
        predictions = coco_demo.compute_prediction(image)
        top_predictions = coco_demo.select_top_predictions(predictions)

        # draw
        #img = coco_demo.overlay_boxes(image, top_predictions)
        img = coco_demo.overlay_mask(image, top_predictions)
        #img = coco_demo.overlay_class_names(img, top_predictions)
        save_path = os.path.join(out_folder, file_name)
        

        # print results
        boxes = top_predictions.bbox.numpy()
        labels = top_predictions.get_field("labels").numpy()  #label = labelList[np.argmax(scores)]
        scores = top_predictions.get_field("mask_scores").numpy()
        masks = top_predictions.get_field("mask").numpy()
        # width,length,_,img = insect_size(masks,img)

        # saving = {
        #     'width': width,
        #     'length': length
        # }


        # with open('/media/jjuergen/LEXA/benchmark.json', 'w') as f:
        #     json.dump(saving,f)

        cv2.imwrite(save_path, img)

	# for i in range(len(boxes)):
        #     print('box:', i, ' label:', labels[i])
        #     x1,y1,x2,y2 = [round(x) for x in boxes[i]] # = map(int, boxes[i])
        #     print('x1,y1,x2,y2:', x1,y1,x2,y2)
