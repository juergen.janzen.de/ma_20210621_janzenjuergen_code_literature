# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
import datetime
import logging
import tempfile
import time
import os
from collections import OrderedDict
import numpy as np
import torch

from tqdm import tqdm

from ..structures.bounding_box import BoxList
from ..utils.comm import is_main_process
from ..utils.comm import scatter_gather
from ..utils.comm import synchronize


from maskrcnn_benchmark.modeling.roi_heads.mask_head.inference import Masker
from maskrcnn_benchmark.structures.boxlist_ops import boxlist_iou


def compute_on_dataset(model, data_loader, device):
    model.eval()
    results_dict = {}
    cpu_device = torch.device("cpu")
    for i, batch in tqdm(enumerate(data_loader)):
        images, targets, image_ids = batch
        images = images.to(device)
        with torch.no_grad():
            output = model(images)
            output = [o.to(cpu_device) for o in output]
        results_dict.update(
            {img_id: result for img_id, result in zip(image_ids, output)}
        )
    return results_dict


def prepare_for_coco_detection(predictions, dataset):
    # assert isinstance(dataset, COCODataset)
    coco_results = []
    for image_id, prediction in enumerate(predictions):
        original_id = dataset.id_to_img_map[image_id]
        if len(prediction) == 0:
            continue

        # TODO replace with get_img_info?
        image_width = dataset.coco.imgs[original_id]["width"]
        image_height = dataset.coco.imgs[original_id]["height"]
        prediction = prediction.resize((image_width, image_height))
        prediction = prediction.convert("xywh")

        boxes = prediction.bbox.tolist()
        scores = prediction.get_field("scores").tolist()
        labels = prediction.get_field("labels").tolist()

        mapped_labels = [dataset.contiguous_category_id_to_json_id[i] for i in labels]

        coco_results.extend(
            [
                {
                    "image_id": original_id,
                    "category_id": mapped_labels[k],
                    "bbox": box,
                    "score": scores[k],
                }
                for k, box in enumerate(boxes)
            ]
        )
    return coco_results


def prepare_for_coco_segmentation(predictions, dataset, maskiou_on):
    import pycocotools.mask as mask_util
    import numpy as np

    masker = Masker(threshold=0.5, padding=1)
    # assert isinstance(dataset, COCODataset)
    coco_results = []
    for image_id, prediction in tqdm(enumerate(predictions)):
        original_id = dataset.id_to_img_map[image_id]
        if len(prediction) == 0:
            continue

        # TODO replace with get_img_info?
        image_width = dataset.coco.imgs[original_id]["width"]
        image_height = dataset.coco.imgs[original_id]["height"]
        prediction = prediction.resize((image_width, image_height))
        masks = prediction.get_field("mask")

        # t = time.time()
        # Masker is necessary only if masks haven't been already resized.
        if list(masks.shape[-2:]) != [image_height, image_width]:
            masks = masker(masks.expand(1, -1, -1, -1, -1), prediction)
            masks = masks[0]
        # logger.info('Time mask: {}'.format(time.time() - t))
        # prediction = prediction.convert('xywh')

        # boxes = prediction.bbox.tolist()
        if maskiou_on:
            scores = prediction.get_field("mask_scores").tolist()
        else:
            scores = prediction.get_field("scores").tolist()
        labels = prediction.get_field("labels").tolist()

        # rles = prediction.get_field('mask')

        rles = [
            mask_util.encode(np.array(mask[0, :, :, np.newaxis], order="F"))[0]
            for mask in masks
        ]
        for rle in rles:
            rle["counts"] = rle["counts"].decode("utf-8")

        mapped_labels = [dataset.contiguous_category_id_to_json_id[i] for i in labels]

        coco_results.extend(
            [
                {
                    "image_id": original_id,
                    "category_id": mapped_labels[k],
                    "segmentation": rle,
                    "score": scores[k],
                }
                for k, rle in enumerate(rles)
            ]
        )
    return coco_results

# def evaluate_predictions_on_coco(
#     coco_gt, coco_results, json_result_file, iou_type="bbox"
# ):
#     import json

#     with open(json_result_file, "w") as f:
#         json.dump(coco_results, f)

#     from pycocotools.cocoeval import COCOeval

#     coco_dt = coco_gt.loadRes(str(json_result_file))
#     coco_eval = COCOeval(coco_gt, coco_dt, iou_type)
#     coco_eval.params.maxDets = [1000,1000,1000]
#     coco_eval.evaluate()
#     coco_eval.accumulate()
#     coco_eval.summarize()
#     return coco_eval


def _accumulate_predictions_from_multiple_gpus(predictions_per_gpu):
    all_predictions = scatter_gather(predictions_per_gpu)
    if not is_main_process():
        return
    # merge the list of dicts
    predictions = {}
    for p in all_predictions:
        predictions.update(p)
    # convert a dict where the key is the index in a list
    image_ids = list(sorted(predictions.keys()))
    if len(image_ids) != image_ids[-1] + 1:
        logger = logging.getLogger("maskrcnn_benchmark.inference")
        logger.warning(
            "Number of images that were gathered from multiple processes is not "
            "a contiguous set. Some images might be missing from the evaluation"
        )

    # convert to a list
    predictions = [predictions[i] for i in image_ids]
    return predictions

class BinaryResults():

    def __init__(self, predictions, groundtruths):
        self.CatIds = groundtruths.coco.getCatIds()
        self.num_predictions = len(predictions)
        self.num_groundtruths = len(groundtruths.coco.getAnnIds())
        self.masker = Masker(threshold=0.5, padding=1)
        self.F1, self.IoU, self.IoG, self.CoG = self.calc_IOU_IOG_COG(predictions, groundtruths)

    def calc_IOU_IOG_COG(self, predictions, groundtruths):
        """
        Claculates the IoU (intersection-over-union), IoG (intersection-over-ground truth), CoG (centre-over-ground truth),and F1-score for each prediction. The results are saved in dicts (Score_dict[image id][class id][result/ number of gronudtruth])
        """
        from pycocotools.mask import encode, merge, frPyObjects, area
        from cv2 import moments
        import operator

        F1_dict = dict()
        IoU_dict = dict()
        IoG_dict = dict()
        CoG_dict = dict()

        #looping through all images
        for imgid, prediction in enumerate(predictions):
            
            org_id = groundtruths.id_to_img_map[imgid]
            width = groundtruths.coco.imgs[org_id]["width"]
            height = groundtruths.coco.imgs[org_id]["height"]
            prediction = prediction.resize((width, height))
            scores = prediction.get_field('mask_scores').numpy()
            mask = prediction.get_field('mask')
            mask = self.masker([mask], [prediction])[0]
            F1_dict[imgid] = dict()
            IoU_dict[imgid] = dict()
            IoG_dict[imgid] = dict()
            CoG_dict[imgid] = dict()
            
            #looping through all possible classes
            for cat in self.CatIds:
                F1_dict[imgid][cat] = dict()
                IoU_dict[imgid][cat] = dict()
                IoG_dict[imgid][cat] = dict()
                CoG_dict[imgid][cat] = dict()
                
                annid = groundtruths.coco.getAnnIds(imgIds = org_id, catIds = cat) #getting the annotaion ids corresponding to the class cat
                gt_binary = list()
                gt_rle = list()
                gt_centroid = list()
                for ann in annid:
                    gt = groundtruths.coco.loadAnns(ann) #loading annotation/ groundtruth
                    gt_binary.append(groundtruths.coco.annToMask(gt[0])) #converting to a binary mask
                    gt_rle.append(groundtruths.coco.annToRLE(gt[0]))# converting to run-lenght-encoding
                    g_M = moments(gt_binary[-1])# claculating moments of the mask see docu of cv2.moments
                    gt_centroid.append(np.array([int(g_M["m10"]/g_M["m00"]), int(g_M["m01"]/g_M["m00"])]))# calculating centroids of the ground truths

                #looping through all predictions
                for j, label in enumerate(prediction.get_field('labels').tolist()):
                    
                    # if predicted class matches class cat than true
                    if groundtruths.contiguous_category_id_to_json_id[label] == cat:
                        
                        d_mask = np.array(mask[j][imgid], order="F")# getting binary mask
                        d_M = moments(d_mask)
                        

                        if not d_M["m00"] == 0:
                            d_center = np.array([int(d_M["m10"]/d_M["m00"]), int(d_M["m01"]/d_M["m00"])])
                        else:
                            d_center = np.array(None)
                        
                        d_mask = encode(d_mask)
                        mask_score = float(scores[j])

                        IOU_list = list()
                        IOG_list = list()
                        COG_list = list()  
                        F1_list = list()

                        #looping through all groundtruths for comparison of each prediction with every groundtruth  
                        for ann, gt_binary_mask, gt_rle_mask, gt_center in zip(annid, gt_binary, gt_rle, gt_centroid):
                            
                            # if centre could be calculated than proside otherwise prediction doesn't match any groundtruth for CoG
                            if not d_center.any() == None:
                                in_mask = gt_binary_mask[d_center[1]][d_center[0]] #if center of pred. is in gt than the respective pixel in the gt should be one/true
                            else:
                                in_mask = False

                            intersection = area(merge([gt_rle_mask, d_mask],intersect=True))
                            union = area(merge([gt_rle_mask, d_mask],intersect=False))
                            IOU = float(intersection/union)
                            IOG = float(intersection/area(gt_rle_mask))
                            F1 = float(2*intersection/(area(gt_rle_mask) + area(d_mask)))

                            # for each gt. save the score and gt-id in a tuple
                            if not (F1 == 0):
                                F1_list.append((F1,ann))

                            if not (IOU == 0):
                                IOU_list.append((IOU,ann))

                            if not (IOG == 0):
                                IOG_list.append((IOG,ann))

                            if in_mask:
                                COG = float(np.linalg.norm((gt_center-d_center)))
                                COG_list.append((COG, ann))
                        
                        if F1_list:
                            F1_tuple = sorted(F1_list, key=operator.itemgetter(0), reverse = True)[0] #getting the best match

                            # if another pred. already matched wiht this ground truth only the one with the higher mask-score is considered a true positive
                            if F1_tuple[1] in F1_dict[imgid][cat].keys():
                                if mask_score > F1_dict[imgid][cat][F1_tuple[1]]['score']:
                                    
                                    F1_key = str(time.time())
                                    F1_dict[imgid][cat][F1_key] = dict()
                                    F1_dict[imgid][cat][F1_key]['score'] = F1_dict[imgid][cat][F1_tuple[1]]['score']
                                    F1_dict[imgid][cat][F1_key]['f1'] = 0

                                    F1_dict[imgid][cat][F1_tuple[1]]['score'] = mask_score
                                    F1_dict[imgid][cat][F1_tuple[1]]['f1'] = F1_tuple[0]
                                
                                else:
                                    F1_key = str(time.time())
                                    F1_dict[imgid][cat][F1_key] = dict()
                                    F1_dict[imgid][cat][F1_key]['score'] = mask_score
                                    F1_dict[imgid][cat][F1_key]['f1'] = 0
                            else:
                                F1_dict[imgid][cat][F1_tuple[1]] = dict()
                                F1_dict[imgid][cat][F1_tuple[1]]['score'] = mask_score
                                F1_dict[imgid][cat][F1_tuple[1]]['f1'] = F1_tuple[0]

                        else:
                            F1_key = str(time.time())
                            F1_dict[imgid][cat][F1_key] = dict()
                            F1_dict[imgid][cat][F1_key]['score'] = mask_score
                            F1_dict[imgid][cat][F1_key]['f1'] = 0

                        if IOU_list:
                            # see F1-score
                            IOU_tuple = sorted(IOU_list, key=operator.itemgetter(0), reverse = True)[0]

                            if IOU_tuple[1] in IoU_dict[imgid][cat].keys():
                                if mask_score > IoU_dict[imgid][cat][IOU_tuple[1]]['score']:
                                    
                                    IoU_key = str(time.time())
                                    IoU_dict[imgid][cat][IoU_key] = dict()
                                    IoU_dict[imgid][cat][IoU_key]['score'] = IoU_dict[imgid][cat][IOU_tuple[1]]['score']
                                    IoU_dict[imgid][cat][IoU_key]['iou'] = 0

                                    IoU_dict[imgid][cat][IOU_tuple[1]]['score'] = mask_score
                                    IoU_dict[imgid][cat][IOU_tuple[1]]['iou'] = IOU_tuple[0]
                                
                                else:
                                    IoU_key = str(time.time())
                                    IoU_dict[imgid][cat][IoU_key] = dict()
                                    IoU_dict[imgid][cat][IoU_key]['score'] = mask_score
                                    IoU_dict[imgid][cat][IoU_key]['iou'] = 0
                            else:
                                IoU_dict[imgid][cat][IOU_tuple[1]] = dict()
                                IoU_dict[imgid][cat][IOU_tuple[1]]['score'] = mask_score
                                IoU_dict[imgid][cat][IOU_tuple[1]]['iou'] = IOU_tuple[0]

                        else:
                            IoU_key = str(time.time())
                            IoU_dict[imgid][cat][IoU_key] = dict()
                            IoU_dict[imgid][cat][IoU_key]['score'] = mask_score
                            IoU_dict[imgid][cat][IoU_key]['iou'] = 0

                        if IOG_list:
                            # see F1-score
                            IOG_tuple = sorted(IOG_list, key=operator.itemgetter(0), reverse = True)[0]

                            if IOG_tuple[1] in IoG_dict[imgid][cat].keys():
                                if mask_score > IoG_dict[imgid][cat][IOG_tuple[1]]['score']:

                                    IoG_key = str(time.time())
                                    IoG_dict[imgid][cat][IoG_key] = dict()
                                    IoG_dict[imgid][cat][IoG_key]['score'] = IoG_dict[imgid][cat][IOG_tuple[1]]['score']
                                    IoG_dict[imgid][cat][IoG_key]['iog'] = 0

                                    IoG_dict[imgid][cat][IOG_tuple[1]]['score'] = mask_score
                                    IoG_dict[imgid][cat][IOG_tuple[1]]['iog'] = IOG_tuple[0]
                                
                                else:
                                    IoG_key = str(time.time())
                                    IoG_dict[imgid][cat][IoG_key] = dict()
                                    IoG_dict[imgid][cat][IoG_key]['score'] = mask_score
                                    IoG_dict[imgid][cat][IoG_key]['iog'] = 0
                            else:
                                IoG_dict[imgid][cat][IOG_tuple[1]] = dict()
                                IoG_dict[imgid][cat][IOG_tuple[1]]['score'] = mask_score
                                IoG_dict[imgid][cat][IOG_tuple[1]]['iog'] = IOG_tuple[0]

                        else:
                            IoG_key = str(time.time())
                            IoG_dict[imgid][cat][IoG_key] = dict()
                            IoG_dict[imgid][cat][IoG_key]['score'] = mask_score
                            IoG_dict[imgid][cat][IoG_key]['iog'] = 0
                        
                        if COG_list:
                            # see F1-score
                            COG_tuple = sorted(COG_list, key=operator.itemgetter(0))[0]

                            if COG_tuple[1] in CoG_dict[imgid][cat].keys():
                                if mask_score > CoG_dict[imgid][cat][COG_tuple[1]]['score']:

                                    CoG_key = str(time.time())
                                    CoG_dict[imgid][cat][CoG_key] = dict()
                                    CoG_dict[imgid][cat][CoG_key]['score'] = CoG_dict[imgid][cat][COG_tuple[1]]['score']
                                    CoG_dict[imgid][cat][CoG_key]['cog'] = width*height

                                    CoG_dict[imgid][cat][COG_tuple[1]]['score'] = mask_score
                                    CoG_dict[imgid][cat][COG_tuple[1]]['cog'] = COG_tuple[0]

                                else:
                                    CoG_key = str(time.time())
                                    CoG_dict[imgid][cat][CoG_key] = dict()
                                    CoG_dict[imgid][cat][CoG_key]['score'] = mask_score
                                    CoG_dict[imgid][cat][CoG_key]['cog'] = width*height
                            else:
                                CoG_dict[imgid][cat][COG_tuple[1]] = dict()
                                CoG_dict[imgid][cat][COG_tuple[1]]['score'] = mask_score
                                CoG_dict[imgid][cat][COG_tuple[1]]['cog'] = COG_tuple[0]

                        else:
                            CoG_key = str(time.time())
                            CoG_dict[imgid][cat][CoG_key] = dict()
                            CoG_dict[imgid][cat][CoG_key]['score'] = mask_score
                            CoG_dict[imgid][cat][CoG_key]['cog'] = width*height
                
                # the information about which gt matches which prediction is not of importance anymore and will be deleted

                F1_dict[imgid][cat]['result'] = [F1_dict[imgid][cat].pop(ann) for ann in F1_dict[imgid][cat].copy().keys()]
                F1_dict[imgid][cat]['num_gt'] = len(annid)

                IoU_dict[imgid][cat]['result'] = [IoU_dict[imgid][cat].pop(ann) for ann in IoU_dict[imgid][cat].copy().keys()]
                IoU_dict[imgid][cat]['num_gt'] = len(annid)

                IoG_dict[imgid][cat]['result'] = [IoG_dict[imgid][cat].pop(ann) for ann in IoG_dict[imgid][cat].copy().keys()]
                IoG_dict[imgid][cat]['num_gt'] = len(annid)


                CoG_dict[imgid][cat]['result'] = [CoG_dict[imgid][cat].pop(ann) for ann in CoG_dict[imgid][cat].copy().keys()]
                CoG_dict[imgid][cat]['num_gt'] = len(annid)

        
        return F1_dict, IoU_dict, IoG_dict, CoG_dict    

    def save(self, save_path_name, iteration):
        """
        Saves the results in a json-file as dictionaries, while using always the same file if the file already exists and order them to the respective iteration
        """
        import json

        if os.path.exists(save_path_name):
            with open(save_path_name,'r') as old_file:
                results = json.load(old_file)
        else:
            results = dict()
            results['F1'] = dict()
            results['IOU'] = dict()
            results['IOG'] = dict()
            results['COG'] = dict()

        results['F1'][iteration] = dict()
        results['IOU'][iteration] = dict()
        results['IOG'][iteration] = dict()
        results['COG'][iteration] = dict()

        results['F1'][iteration].update(self.F1)
        results['IOU'][iteration].update(self.IoU)
        results['IOG'][iteration].update(self.IoG)
        results['COG'][iteration].update(self.CoG)

        with open(save_path_name,'w+') as save:
            json.dump(results, save)


def inference(
    model,
    data_loader,
    device="cuda",
    output_folder=None,
    model_name = '.pth'
):

    # convert to a torch.device for efficiency
    device = torch.device(device)
    num_devices = (
        torch.distributed.deprecated.get_world_size()
        if torch.distributed.deprecated.is_initialized()
        else 1
    )
    logger = logging.getLogger("maskrcnn_benchmark.inference")
    dataset = data_loader.dataset
    logger.info("Start evaluation on {} images".format(len(dataset)))
    start_time = time.time()
    predictions = compute_on_dataset(model, data_loader, device)
    # wait for all processes to complete before measuring the time
    synchronize()
    total_time = time.time() - start_time
    total_time_str = str(datetime.timedelta(seconds=total_time))
    logger.info(
        "Total inference time: {} ({} s / img per device, on {} devices)".format(
            total_time_str, total_time * num_devices / len(dataset), num_devices
        )
    )

    predictions = _accumulate_predictions_from_multiple_gpus(predictions)
    if not is_main_process():
        return
    logger.info('Calculating metrics')
    eval_prediction = BinaryResults(predictions, dataset)
    
    file_name = 'eval_metric.json'
    save_path = os.path.join(output_folder, file_name)
    it = int(model_name.split('_')[1].split('.')[0])
    eval_prediction.save(save_path, it)

    return eval_prediction, predictions

