# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
# Set up custom environment before nearly anything else is imported
# NOTE: this should be the first import (no not reorder)
from maskrcnn_benchmark.utils.env import setup_environment  # noqa F401 isort:skip

import argparse
import os
from logging import getLogger
from tqdm import tqdm
import torch
from maskrcnn_benchmark.config import cfg
from maskrcnn_benchmark.data import make_data_loader
from maskrcnn_benchmark.engine.inference_costum import inference
from maskrcnn_benchmark.modeling.detector import build_detection_model
from maskrcnn_benchmark.utils.checkpoint import DetectronCheckpointer
from maskrcnn_benchmark.utils.collect_env import collect_env_info
from maskrcnn_benchmark.utils.comm import synchronize, get_rank
from maskrcnn_benchmark.utils.logger import setup_logger
from maskrcnn_benchmark.utils.miscellaneous import mkdir


def main(model_file_name='.pth', num_gpus=None):
    distributed = num_gpus > 1
    save_dir = ""

    if not getLogger("maskrcnn_benchmark").hasHandlers():
        logger = setup_logger("maskrcnn_benchmark", save_dir, get_rank())
    else:
        logger = getLogger("maskrcnn_benchmark")
    
    logger.info("Using {} GPUs".format(num_gpus))
    logger.info(cfg)

    logger.info("Collecting env info (might take some time)")
    logger.info("\n" + collect_env_info())

    model = build_detection_model(cfg)
    model.to(cfg.MODEL.DEVICE)

    output_dir = cfg.OUTPUT_DIR
    checkpointer = DetectronCheckpointer(cfg, model, save_dir=output_dir)
    _ = checkpointer.load(cfg.MODEL.WEIGHT)

    iou_types = ("bbox",)
    if cfg.MODEL.MASK_ON:
        iou_types = iou_types + ("segm",)

    output_folders = [None] * len(cfg.DATASETS.TEST)# it is possible to test on multiple testsets
    # generating actual output directories depending on the name of the folder in which the model is saved, the name of the testset, and root save folder given by the config-file
    if cfg.OUTPUT_DIR:
        dataset_names = cfg.DATASETS.TEST
        for idx, dataset_name in enumerate(dataset_names):
            output_folder = os.path.join(cfg.OUTPUT_DIR, dataset_name)
            mkdir(output_folder)
            output_folders[idx] = output_folder
    data_loaders_val = make_data_loader(cfg, is_train=False, is_distributed=distributed)
    for output_folder, data_loader_val in zip(output_folders, data_loaders_val):
        inference(
            model,
            data_loader_val,
            device=cfg.MODEL.DEVICE,
            output_folder=output_folder,
            model_name=model_file_name
        )
        synchronize()

def main_in_loop():
    """
    Puting the main into a loop. At the end there is a saved eval_meter.json for every trainset ,which containes the test results of every model corresponding to the respective trainset.
    """
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Inference")
    parser.add_argument(
        "--config-file",
        default="/private/home/fmassa/github/detectron.pytorch_v2/configs/e2e_faster_rcnn_R_50_C4_1x_caffe2.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    args = parser.parse_args()

    num_gpu = int(os.environ["WORLD_SIZE"]) if "WORLD_SIZE" in os.environ else 1
    distributed = num_gpu > 1

    if distributed:
        torch.cuda.set_device(args.local_rank)
        torch.distributed.deprecated.init_process_group(
            backend="nccl", init_method="env://"
        )

    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()
    
    crop_folder = cfg.MODEL.WEIGHT
    org_outputdir = cfg.OUTPUT_DIR
    for crop in tqdm(os.listdir(crop_folder), maxinterval=len(os.listdir(crop_folder))):
        combinations = os.path.join(crop_folder, crop)
        for combi in tqdm(os.listdir(combinations),maxinterval=len(os.listdir(combinations))):
            combi_path = os.path.join(combinations, combi)
            for model in tqdm(os.listdir(combi_path),maxinterval=len(os.listdir(combi_path))):
                if 'model' in model:
                    cfg.defrost()
                    cfg.MODEL.WEIGHT = os.path.join(combi_path, model)
                    cfg.OUTPUT_DIR = os.path.join(org_outputdir, crop, combi)
                    cfg.freeze()
                    main(model,num_gpu)

def main_in_loop_LR():
    """
    same as main_in_loop but aditional looping through the different learning-rates or datsetsizes
    """
    LR = [40,60] # learning rates or datasetsizes
    basepath = '/home/jjuergen/Desktop/masterarbeit/Code/MSRCNN/maskscoring_rcnn/models/Rot180FlipVert_Res_'
    basesave = 'validations/Rot180FlipVert_Res_'

   
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Inference")
    parser.add_argument(
        "--config-file",
        default="/private/home/fmassa/github/detectron.pytorch_v2/configs/e2e_faster_rcnn_R_50_C4_1x_caffe2.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    args = parser.parse_args()

    num_gpu = int(os.environ["WORLD_SIZE"]) if "WORLD_SIZE" in os.environ else 1
    distributed = num_gpu > 1

    if distributed:
        torch.cuda.set_device(args.local_rank)
        torch.distributed.deprecated.init_process_group(
            backend="nccl", init_method="env://"
        )

    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()

    for l in LR:
        cfg.defrost()
        cfg.OUTPUT_DIR = basesave + '{0}/'.format(l)
        cfg.MODEL.WEIGHT = basepath + '{0}'.format(l)
        cfg.freeze()

        for ll in LR:
            cfg.defrost()
            cfg.DATASETS.TEST = ("coco_test_res{0}".format(ll),)
            cfg.freeze()
        
            crop_folder = cfg.MODEL.WEIGHT
            org_outputdir = cfg.OUTPUT_DIR
            for crop in tqdm(os.listdir(crop_folder), maxinterval=len(os.listdir(crop_folder))):
                combinations = os.path.join(crop_folder, crop)
                for combi in tqdm(os.listdir(combinations),maxinterval=len(os.listdir(combinations))):
                    combi_path = os.path.join(combinations, combi)
                    for model in tqdm(os.listdir(combi_path),maxinterval=len(os.listdir(combi_path))):
                        if 'model' in model:
                            cfg.defrost()
                            cfg.MODEL.WEIGHT = os.path.join(combi_path, model)
                            cfg.OUTPUT_DIR = os.path.join(org_outputdir, crop, combi)
                            cfg.freeze()
                            main(model,num_gpu)
if __name__ == "__main__":
    main_in_loop()
