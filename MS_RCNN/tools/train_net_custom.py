# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
r"""
Basic training script for PyTorch
"""

# Set up custom environment before nearly anything else is imported
# NOTE: this should be the first import (no not reorder)
from maskrcnn_benchmark.utils.env import setup_environment  # noqa F401 isort:skip

import argparse
import os
import itertools
import json
import traceback
from math import ceil
from logging import FileHandler, getLogger
from numpy import arange
import torch
from maskrcnn_benchmark.config import cfg
from maskrcnn_benchmark.data import make_data_loader
from maskrcnn_benchmark.solver import make_lr_scheduler
from maskrcnn_benchmark.solver import make_optimizer
from maskrcnn_benchmark.engine.inference import inference
from maskrcnn_benchmark.engine.trainer import do_train
from maskrcnn_benchmark.modeling.detector import build_detection_model
from maskrcnn_benchmark.utils.checkpoint import DetectronCheckpointer
from maskrcnn_benchmark.utils.collect_env import collect_env_info
from maskrcnn_benchmark.utils.comm import synchronize, get_rank
from maskrcnn_benchmark.utils.imports import import_file
from maskrcnn_benchmark.utils.logger import setup_logger
from maskrcnn_benchmark.utils.miscellaneous import mkdir


def train(config, local_rank, distributed, combination):
    model = build_detection_model(cfg)
    device = torch.device(cfg.MODEL.DEVICE) #use the CPU or GPU to train
    model.to(device)

    optimizer = make_optimizer(config, model) #used to update/ optimize weights
    scheduler = make_lr_scheduler(config, optimizer) #in case a learning rate scheduel is used

    #manages loadout for each device GPU/CPU used; in thesis only one GPU used therefore never used
    if distributed:
        model = torch.nn.parallel.deprecated.DistributedDataParallel(
            model, device_ids=[local_rank], output_device=local_rank,
            # this should be removed if we update BatchNorm stats
            broadcast_buffers=False,
        )

    arguments = {}
    arguments["iteration"] = 0

    output_dir = cfg.OUTPUT_DIR

    save_to_disk = get_rank() == 0
    checkpointer = DetectronCheckpointer(
        config, model, optimizer, scheduler, output_dir, save_to_disk
    )

    #loads the weights and training parameters (all also previous learning rate!) provided this data is available otherwise nothing happens 
    extra_checkpoint_data = checkpointer.load(cfg.MODEL.WEIGHT) 
    arguments.update(extra_checkpoint_data)

    # manages mini batches
    data_loader = make_data_loader(
        config,
        is_train=True,
        is_distributed=distributed,
        start_iter=arguments["iteration"],
        combination=combination
    )

    checkpoint_period = config.SOLVER.CHECKPOINT_PERIOD

    do_train(
        model,
        data_loader,
        optimizer,
        scheduler,
        checkpointer,
        device,
        checkpoint_period,
        arguments,
    )

    return model

def main():
    """
    Traines the algorithm on all datasets in a loop. After each iteration the weights are saved as .pth and the training restarts with the next dataset.
    """
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Training")
    parser.add_argument(
        "--config-file",
        default="",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument(
        "--skip-test",
        dest="skip_test",
        help="Do not test the final model",
        action="store_true",
    )
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    args = parser.parse_args()

    num_gpus = int(os.environ["WORLD_SIZE"]) if "WORLD_SIZE" in os.environ else 1
    args.distributed = num_gpus > 1

    if args.distributed:
        torch.cuda.set_device(args.local_rank)
        torch.distributed.deprecated.init_process_group(
            backend="nccl", init_method="env://"
        )

    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()

    batch_list = [] #batch sizes for each dataset
    output_dir_list = [] #output directory for each dataset to save the weights/model
    combinations = [] # the respective datasets as combination of the various subsets

    # filling the three previous lists
    if cfg.DATASETS.COMBI:

        mainsets = os.listdir(cfg.DATASETS.PARENT_FOLDER) # lists all cropping operations
        mainsets.sort()
        assert len(mainsets) == len(cfg.DATASETS.IMS_PER_BATCH)
        indexer = 0

        for sets in mainsets:
            img_per_batch = [cfg.DATASETS.IMS_PER_BATCH[indexer]]
            indexer += 1
            mainset_path = os.path.join(cfg.DATASETS.PARENT_FOLDER, sets)
            # lists all subsets of the corresponding cropping operations
            subsets = os.listdir(mainset_path) 
            # folder None (pure cropping without further augmentaions) can be removed, since is in all combinations by default 
            subsets.remove(cfg.DATASETS.DEFAULT_FOLDER) 
            subsets = [
                os.path.join(mainset_path, sub) for sub in subsets
            ] # converts subsets to directory of the subsets

            for i in range(len(subsets)+1):
                subcombi = list(itertools.combinations(subsets , i)) # all possible combinations
                
                #adds default subset to each combination
                if cfg.DATASETS.DEFAULT_FOLDER:
                    for i in range(len(subcombi)):
                        sub = list(subcombi[i])
                        sub.append(os.path.join(mainset_path, cfg.DATASETS.DEFAULT_FOLDER))
                        subcombi[i] = tuple(sub)

                combinations += subcombi
                batch_list += img_per_batch * len(subcombi) #batchsize is the same for all combinations since they use the same cropping operation

        # creating list of output directory for each combination
        for combi in combinations:
            common_dir = os.path.basename(os.path.dirname(combi[0]))
            common_dir = os.path.join(cfg.OUTPUT_DIR, common_dir)
            folder=''            
            output = os.path.join(common_dir, folder)
            output_dir_list.append(output + '/')
                
    
    else:
        combinations.append(None)
        output_dir_list.append(cfg.OUTPUT_DIR)
        batch_list.append(cfg.SOLVER.IMS_PER_BATCH)

    oom_count = 0 # oom := out-of-memory
    # actuall loop for training
    for combi, output_dir, batch_size in zip(combinations, output_dir_list, batch_list):

        # decomment to train only certain datasets
        # curent_crop = os.path.basename(os.path.dirname(combi[0]))
        # save_folder = os.path.basename(os.path.dirname(output_dir))
        
        # if (curent_crop == 'crop2575') and (save_folder == 'Rot180FlipVertNone'):
        #     pass
        # else:
        #     continue
        
        
        if combi is not None:
            cfg.defrost()
            cfg.SOLVER.IMS_PER_BATCH = batch_size
            cfg.OUTPUT_DIR = output_dir
            cfg.freeze()

        # checks if an model has been trained already. If this is the case and the model is trained more iterations than requested then this training will be skiped. This prevents subsequent errors which would break the loop.
        if os.path.exists(output_dir):
            file_name = 'meter.json'
            with open(os.path.join(output_dir, file_name),'r') as f:
                meter = json.load(f)
            if cfg.SOLVER.MAX_ITER <= len(meter['loss']):
                error_massage = 'There is already a model which is trained for {0}iterations {0} according to meter.json. Requested iterations: {1}\n This leads to an error during training. Remove all files from this directory {2}'.format(len(meter['loss']), cfg.SOLVER.MAX_ITER,output_dir)

                save_path = os.path.commonpath(output_dir_list)
                file_name = "Error_Report.txt"
                error_path = os.path.join(save_path, file_name)
                
                if os.path.exists(error_path):
                    f = open(error_path,"a")
                else:
                    f = open(error_path, "w+")

                f.write(error_massage)
                f.close()
                continue


        if output_dir:
            mkdir(output_dir)

        # TODO: Logger-file is saved only once at the first iteration. Removing this if-condition may resolve this
        if not getLogger("maskrcnn_benchmark").hasHandlers():
            logger = setup_logger("maskrcnn_benchmark", output_dir, get_rank())
        
        logger.info("Using {} GPUs".format(num_gpus))
        logger.info(args)

        logger.info("Collecting env info (might take some time)")
        logger.info("\n" + collect_env_info())

        logger.info("Loaded configuration file {}".format(args.config_file))
        with open(args.config_file, "r") as cf:
            config_str = "\n" + cf.read()
            logger.info(config_str)
        logger.info("Running with config:\n{}".format(cfg))

        for handler in logger.handlers:
            if isinstance(handler, FileHandler):
                logger.removeHandler(handler)

        #Tries to train 3 times otherwise there is probably an oom and this training will be skiped
        while True:
            try:
                model = train(cfg, args.local_rank, args.distributed, combi)
                
                # clear memory
                del model
                torch.cuda.empty_cache()
                oom_count = 0
                break

            except (RuntimeError, AssertionError):
                torch.cuda.empty_cache()
                oom_count += 1

                if oom_count == 3:
                    save_path = os.path.commonpath(output_dir_list)
                    file_name = "Error_Report.txt"
                    error_path = os.path.join(save_path, file_name)
                    
                    if os.path.exists(error_path):
                        f = open(error_path,"a")
                    else:
                        f = open(error_path, "w+")

                    f.write("\n\n" + output_dir + "\n")
                    f.write(traceback.format_exc())
                    f.close()
                    oom_count += 0
                    break

def main_LR():
    """
    Basicly the same main() looped through all learning rates tested for the thesis.
    """
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Training")
    parser.add_argument(
        "--config-file",
        default="",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument(
        "--skip-test",
        dest="skip_test",
        help="Do not test the final model",
        action="store_true",
    )
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    args = parser.parse_args()

    num_gpus = int(os.environ["WORLD_SIZE"]) if "WORLD_SIZE" in os.environ else 1
    args.distributed = num_gpus > 1

    if args.distributed:
        torch.cuda.set_device(args.local_rank)
        torch.distributed.deprecated.init_process_group(
            backend="nccl", init_method="env://"
        )

    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()

    learnrates = arange(0.0002, 0.0011,0.0001) #0.0001 is skiped this corresponds the learning rate in the experimantal block 'augmentations'

    for lr in learnrates:
        cfg.defrost()
        cfg.SOLVER.BASE_LR=float(lr)
        cfg.OUTPUT_DIR="models/Crop2575_LR{0}/".format(int(lr*10000))
        cfg.freeze()


        batch_list = [] #batch sizes for each dataset
        output_dir_list = [] #output directory for each dataset to save the weights/model
        combinations = [] # the respective datasets as combination of the various subsets

        # filling the three previous lists
        if cfg.DATASETS.COMBI:

            mainsets = os.listdir(cfg.DATASETS.PARENT_FOLDER) # lists all cropping operations
            mainsets.sort()
            assert len(mainsets) == len(cfg.DATASETS.IMS_PER_BATCH)
            indexer = 0

            for sets in mainsets:
                img_per_batch = [cfg.DATASETS.IMS_PER_BATCH[indexer]]
                indexer += 1
                mainset_path = os.path.join(cfg.DATASETS.PARENT_FOLDER, sets)
                # lists all subsets of the corresponding cropping operations
                subsets = os.listdir(mainset_path) 
                # folder None (pure cropping without further augmentaions) can be removed, since is in all combinations by default 
                subsets.remove(cfg.DATASETS.DEFAULT_FOLDER) 
                subsets = [
                    os.path.join(mainset_path, sub) for sub in subsets
                ] # converts subsets to directory of the subsets

                for i in range(len(subsets)+1):
                    subcombi = list(itertools.combinations(subsets , i)) # all possible combinations
                    
                    #adds default subset to each combination
                    if cfg.DATASETS.DEFAULT_FOLDER:
                        for i in range(len(subcombi)):
                            sub = list(subcombi[i])
                            sub.append(os.path.join(mainset_path, cfg.DATASETS.DEFAULT_FOLDER))
                            subcombi[i] = tuple(sub)

                    combinations += subcombi
                    batch_list += img_per_batch * len(subcombi) #batchsize is the same for all combinations since they use the same cropping operation

            # creating list of output directory for each combination
            for combi in combinations:
                common_dir = os.path.basename(os.path.dirname(combi[0]))
                common_dir = os.path.join(cfg.OUTPUT_DIR, common_dir)
                folder=''            
                output = os.path.join(common_dir, folder)
                output_dir_list.append(output + '/')
                    
        
        else:
            combinations.append(None)
            output_dir_list.append(cfg.OUTPUT_DIR)
            batch_list.append(cfg.SOLVER.IMS_PER_BATCH)

        oom_count = 0 # oom := out-of-memory
        # actuall loop for training
        for combi, output_dir, batch_size in zip(combinations, output_dir_list, batch_list):

            # decomment to train only certain datasets
            # curent_crop = os.path.basename(os.path.dirname(combi[0]))
            # save_folder = os.path.basename(os.path.dirname(output_dir))
            
            # if (curent_crop == 'crop2575') and (save_folder == 'Rot180FlipVertNone'):
            #     pass
            # else:
            #     continue
            
            
            if combi is not None:
                cfg.defrost()
                cfg.SOLVER.IMS_PER_BATCH = batch_size
                cfg.OUTPUT_DIR = output_dir
                cfg.freeze()

            # checks if an model has been trained already. If this is the case and the model is trained more iterations than requested then this training will be skiped. This prevents subsequent errors which would break the loop.
            if os.path.exists(output_dir):
                file_name = 'meter.json'
                with open(os.path.join(output_dir, file_name),'r') as f:
                    meter = json.load(f)
                if cfg.SOLVER.MAX_ITER <= len(meter['loss']):
                    error_massage = 'There is already a model which is trained for {0}iterations {0} according to meter.json. Requested iterations: {1}\n This leads to an error during training. Remove all files from this directory {2}'.format(len(meter['loss']), cfg.SOLVER.MAX_ITER,output_dir)

                    save_path = os.path.commonpath(output_dir_list)
                    file_name = "Error_Report.txt"
                    error_path = os.path.join(save_path, file_name)
                    
                    if os.path.exists(error_path):
                        f = open(error_path,"a")
                    else:
                        f = open(error_path, "w+")

                    f.write(error_massage)
                    f.close()
                    continue


            if output_dir:
                mkdir(output_dir)

            # TODO: Logger-file is saved only once at the first iteration. Removing this if-condition may resolve this
            if not getLogger("maskrcnn_benchmark").hasHandlers():
                logger = setup_logger("maskrcnn_benchmark", output_dir, get_rank())
            
            logger.info("Using {} GPUs".format(num_gpus))
            logger.info(args)

            logger.info("Collecting env info (might take some time)")
            logger.info("\n" + collect_env_info())

            logger.info("Loaded configuration file {}".format(args.config_file))
            with open(args.config_file, "r") as cf:
                config_str = "\n" + cf.read()
                logger.info(config_str)
            logger.info("Running with config:\n{}".format(cfg))

            for handler in logger.handlers:
                if isinstance(handler, FileHandler):
                    logger.removeHandler(handler)

            #Tries to train 3 times otherwise there is probably an oom and this training will be skiped
            while True:
                try:
                    model = train(cfg, args.local_rank, args.distributed, combi)
                    
                    # clear memory
                    del model
                    torch.cuda.empty_cache()
                    oom_count = 0
                    break

                except (RuntimeError, AssertionError):
                    torch.cuda.empty_cache()
                    oom_count += 1

                    if oom_count == 3:
                        save_path = os.path.commonpath(output_dir_list)
                        file_name = "Error_Report.txt"
                        error_path = os.path.join(save_path, file_name)
                        
                        if os.path.exists(error_path):
                            f = open(error_path,"a")
                        else:
                            f = open(error_path, "w+")

                        f.write("\n\n" + output_dir + "\n")
                        f.write(traceback.format_exc())
                        f.close()
                        oom_count += 0
                        break

if __name__ == "__main__":
    main()
    #main_LR()
