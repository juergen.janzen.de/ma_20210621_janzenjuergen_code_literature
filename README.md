# Development of a segmentation algorithm for automatic monitoring of an insect production

## Abstract
The production of insects is a viable solution to provide a growing world population with sufficient food. Monitoring is needed for automated production, which ensures quality and safety. There are image processing technologies in use to monitor insect production facilities. However, these technologies do not incorporate the state-of-the-art image processing. Therefore, the instance segmentation algorithm Mask Scoring R-CNN (a machine-learning algorithm) is utilized,which has not been used in this context before. Since there are no publicly available datasets for the use case of insect production, a dataset is created consisting of six images (1022 instances) of a real-world cricket production facility. Furthermore, image augmentation is applied on the set to solve the problem of limited data. The augmentations are in detail cropping, flipping and rotating in different variations.  it is shown that complex semantic information can be inferred from an image. Using the segmentation algorithm Mask Scoring R-CNN and post-processing of the segmentations, the number, width and length of the crickets are inferred. Mask Scoring R-CNN and the presented methods are suitable for monitoring in an insect production facility.

## Overview Folder and Pointers
<details>
<summary>CLoDSA</summary>

Here are the files which are needed to use the libary [CLoDSA](https://github.com/joheras/CLoDSA) to augement the dataset.
</details>

<details>
<summary>Dataset</summary>

This contains the images of the train, validations and test set as well the various annotation-files (abc.json), therein the _allinsect.json_ corresponds to the annotations with three classes and _onlyinsect.json_ corresponds to the annotations with one class. The _annotations.json_ is needed for the augmentation process and the _AchetaDomesticusProduction.json_ is the original annotation file created with [_Coco Annotator_](https://github.com/jsbroks/coco-annotator).
</details>

<details>
<summary>Evaluation</summary>

This folder contains various files to analyze the training and test results saved as JSON-file (meter.json for training eval_metrtic.json for testing)
</details>

<details>
<summary>Literature</summary>

All PDFs of the literature used in the thesis
</details>

<details>
<summary>MS_RCNN</summary>

The Code for [_Mask Scoring R-CNN_](https://github.com/zjhuang22/maskscoring_rcnn). For installation, look into the repository of the original authors or look at MS_RCNN/INSTALL.md. Furthermore, a basic tutorial can be found here: [Programmer Help](https://programmer.help/blogs/training-your-own-dataset-with-mask-scoring-rcnn.html). However, some changes has been done to the original code, accordingly in MS_RCNN/README.md are some pointers regarding the right usage of the algorithm.
</details>
